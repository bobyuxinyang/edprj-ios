//
//  ProjectTableViewCell.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *textLabel;
@property (nonatomic, strong) IBOutlet UILabel *detailTextLabel;

-(void) setupProjectInfo: (NSDictionary*)projectInfo;
@end
