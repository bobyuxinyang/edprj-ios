//
//  ProjectProgressIndicator.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectProgressIndicator : UIView
@property (nonatomic) NSInteger unfinishedCount;
@property (nonatomic) NSInteger finishedCount;
@property (nonatomic) NSInteger expiredCount;

- (id) initWithUnfinishedCount: (NSInteger)unfinishedCount FinishedCount: (NSInteger)finishedCount exipredCount: (NSInteger)expiredCount;
@end
