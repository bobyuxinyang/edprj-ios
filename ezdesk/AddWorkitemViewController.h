//
//  AddWorkitemViewController.h
//  ezdesk
//
//  Created by Bruce Yang on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    EzDeskProjectTasksSourceType,
    EzDeskMyUnfinishedWorkitemsSourceType,
    EzDeskMyAssignWorkitemsSourceType,
} EzDeskAddWorkitemSourceType;

@interface AddWorkitemViewController : UIViewController
@property (nonatomic, strong) NSString *projectId;
@property (nonatomic) BOOL isOwner;
@property (nonatomic) EzDeskAddWorkitemSourceType sourceType;
@property (nonatomic, strong) UIViewController *sourceController;
@end
