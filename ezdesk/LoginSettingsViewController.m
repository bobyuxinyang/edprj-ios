//
//  LoginSettingsViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginSettingsViewController.h"
#import "ASIHttpRequest.h"
#import "EzServerInfo.h"
#import "AppConfig.h"
#import "EzDeskService.h"

typedef enum {
    ServerAddressAvailable = 0,
    ServerAddressNotAvailable = 1,
    ServerAddressEditing = 2,
    ServerAddressChecking = 3,    
} ServerAddressStatus;


@interface LoginSettingsViewController() <UITextFieldDelegate, EzDeskServiceDelegate>
@property (weak, nonatomic) IBOutlet UITextField *serverAddressTextfield;
@property (weak, nonatomic) IBOutlet UILabel *serverStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (strong, nonatomic) NSString* currentServerAddress;
@property (strong, nonatomic) NSString* shortName;
@property (nonatomic) BOOL isEditting;

- (void) setupCurrentServerAddressStatus: (ServerAddressStatus)status;
- (IBAction)setupDefaultServer:(id)sender;
- (void) setupResetButton;
@end

@implementation LoginSettingsViewController
@synthesize serverAddressTextfield = _serverAddressTextfield;
@synthesize serverStatusLabel = _serverStatusLabel;
@synthesize resetButton = _resetButton;
@synthesize currentServerAddress = _currentServerAddress;
@synthesize shortName = _shortName;
@synthesize isEditting = _isEditting;
@synthesize service = _service;

#pragma mark -- Properties
- (EzDeskService*) service
{
    if (_service == nil) {
        _service = [[EzDeskService alloc]init];
        _service.delegate = self;
    }
    return _service;
}

- (void)endEditServerAddress
{
    self.isEditting = NO;
    [self setupCurrentServerAddressStatus:ServerAddressChecking];
    [self.serverAddressTextfield resignFirstResponder];
    
    [self.service checkIfServerAvailable:self.serverAddressTextfield.text];
    [self setupResetButton];
}

- (void) setupResetButton 
{
    [self.resetButton setHidden: [self.serverAddressTextfield.text isEqualToString: self.currentServerAddress]];
}

- (void) setupCurrentServerAddressStatus: (ServerAddressStatus)status
{
    switch (status) {
        case ServerAddressAvailable:
            self.serverStatusLabel.text = @"地址正确";                  
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
            break;
        case ServerAddressEditing:
            self.serverStatusLabel.text = @"";
            [self.navigationItem.rightBarButtonItem setEnabled:NO];            
            break;
        case ServerAddressChecking:
            self.serverStatusLabel.text = @"验证中...";
            [self.navigationItem.rightBarButtonItem setEnabled:NO];            
            break;
        case ServerAddressNotAvailable:
            self.serverStatusLabel.text = @"地址无效，请检查输入是否有误！";
            [self.navigationItem.rightBarButtonItem setEnabled:NO];            
            break;
    }
}


- (IBAction)setupCurrentServerAddress:(id)sender {
    NSString* currentServerAddress = [AppUtil sharedInstance].serviceAddress;
    NSString* currentShortName = [[NSUserDefaults standardUserDefaults] objectForKey:ORG_SHORT_NAME];    
    if (currentServerAddress == nil) {
        [self setupDefaultServer: self];
        currentServerAddress = self.serverAddressTextfield.text;
    } else {
        self.serverAddressTextfield.text = currentServerAddress;
        self.shortName = currentShortName;
        [self setupCurrentServerAddressStatus:ServerAddressAvailable];
    }
    self.currentServerAddress = currentServerAddress;
    [self setupResetButton];
    [self.serverAddressTextfield resignFirstResponder];
}


- (IBAction)setupDefaultServer:(id)sender {
    // set default server
    NSString *defaultServerAddress = [[AppConfig sharedInstance] GetValueByKey:SERVER_ADDRESS];
    self.shortName = [[AppConfig sharedInstance] GetValueByKey:ORG_SHORT_NAME];    
    self.serverAddressTextfield.text = defaultServerAddress;
    [self setupCurrentServerAddressStatus:ServerAddressAvailable];  
    [self setupResetButton];
    [self.serverAddressTextfield resignFirstResponder];
}



- (IBAction)editFinished:(id)sender {
    
    if (self.isEditting) {
        [self.serverAddressTextfield endEditing: YES];
        [self endEditServerAddress];
    } else {
        // 把当前服务器地址写到user defaults中去
        // 1. ServerAddress
        NSString *serverAddress = self.serverAddressTextfield.text;
        [AppUtil sharedInstance].serviceAddress = serverAddress;
        [[NSUserDefaults standardUserDefaults]setObject:self.shortName forKey:ORG_SHORT_NAME];    
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark --view lifecycle

- (void)viewDidLoad 
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewDidUnload
{
    [self setServerAddressTextfield:nil];
    [self setServerStatusLabel:nil];
    [self setResetButton:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self setupCurrentServerAddress:self];
    [super viewDidAppear:animated];
}

#pragma mark --UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{   
    [self setupCurrentServerAddressStatus:ServerAddressEditing];
    [self.navigationItem.rightBarButtonItem setEnabled: YES];
    self.isEditting = YES;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{   
    [self endEditServerAddress];    
    return YES;
}


#pragma mark -- EzDeskService Delegate
- (void) service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_CHECK_IF_SERVER_AVAILABLE]) {
        EzServerInfo *info = [[EzServerInfo alloc] initWithJsonData: jsonData];                                
        self.shortName = info.shortName;
        [self setupCurrentServerAddressStatus: ServerAddressAvailable];
        
        [self editFinished: self];
    }
}

- (void) service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_CHECK_IF_SERVER_AVAILABLE]) {    
        [self setupCurrentServerAddressStatus: ServerAddressNotAvailable];
    }
}

@end
