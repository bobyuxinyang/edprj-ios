//
//  WorkitemDetailViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WorkitemDetailViewController.h"
#import "EzDeskService.h"
#import "UIColor+WebColor.h"
#import "Datetime+FormattedString.h"
#import "WorkitemDescriptionViewController.h"
#import "EditWorkitemTitleViewController.h"
#import "EditProjectAndTaskTableViewController.h"
#import "EditWorkitemOwnerViewController.h"
#import "PlanOnDateSelector.h"


#define SECTION_COUNT 3

#define SECTION_BASIC_INFO_INDEX 0
#define SECTION_BASIC_INFO_ROWCOUNT 3
    #define ROW_TITLE_INDEX 0
    #define ROW_PROJECT_INFO_INDEX 1
    #define ROW_OWNER_INFO_INDEX 2

#define SECTION_TIME_INFO_INDEX 1
#define SECTION_TIME_INFO_ROWCOUNT 2
    #define ROW_CREATE_ON_INDEX 0
    #define ROW_PLAN_OR_FINISH_ON_INDEX 1

#define SECTION_DESCRIPTION_INFO_INDEX 2
#define SECTION_DESCRIPTION_INFO_ROWCOUNT 1
    #define ROW_DESCRIPTION_INFO_INDEX 0

#define HEADER_HEIGHT_WITH_BUTTON 73.0f
#define HEADER_HEIGHT_WITHOUT_BUTTON 29.0f

@interface WorkitemDetailViewController()

@property (strong, nonatomic) IBOutlet UIView *statusBar;
@property (strong, nonatomic) IBOutlet UILabel *workitemStatusLabel;
@property (strong, nonatomic) IBOutlet UIButton *workitemActionButton;
@property (strong, nonatomic) IBOutlet UIView *headerContainer;

@property (strong, nonatomic) EzDeskService *service;
@property (strong, nonatomic) PlanOnDateSelector *planOnDateSelector;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;

- (void)setupStatusAndButtons;
@end

@implementation WorkitemDetailViewController

@synthesize workitemInfo = _workitemInfo;
@synthesize statusBar = _statusBar;
@synthesize workitemStatusLabel = _workitemStatusLabel;
@synthesize workitemActionButton = _workitemActionButton;
@synthesize headerContainer = _headerContainer;
@synthesize service = _service;
@synthesize planOnDateSelector = _planOnDateSelector;
@synthesize loadingIndicatorView = _loadingIndicatorView;
@synthesize isNotificationMode = _isNotificationMode;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)setWorkitemInfo:(NSDictionary *)workitemInfo
{
    if (![_workitemInfo isEqual:workitemInfo]) {
        _workitemInfo = workitemInfo;
        [self setupStatusAndButtons];
        [self.tableView reloadData];
    }
}

- (void)fetchWorkitemInfoWithWorkitemId: (NSString *)workitemId
{    
    [self.loadingIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.service fetchWorkitemInfoWithWorkitemId: workitemId];
}

- (void)setupStatusAndButtons
{
    UIColor *barColor;
    NSString *statusInfo, *buttonText;
    switch ([[self.workitemInfo objectForKey:@"Status"] intValue]) {
        case WorkitemStatusOnGoing:
        {
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_ON_GOING];
            statusInfo = @"任务正在进行中...";
            buttonText = @"完成任务";
            [self.workitemActionButton setBackgroundImage: [UIImage imageNamed:@"btn-green.png"]
                                                 forState:UIControlStateNormal];
            break;
        }
        case WorkitemStatusFinished:
        {
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_FINISHED];
            statusInfo = @"任务已经完成";
            buttonText = nil;
            break;
        }
        case WorkitemStatusExpired:
        {
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_EXPIRED];
            statusInfo = @"任务超过了预计期限";
            buttonText = @"完成任务";
            [self.workitemActionButton setBackgroundImage: [UIImage imageNamed:@"btn-red.png"]
                                                 forState:UIControlStateNormal];            
            break;
        }
        case WorkitemStatusWaitingForConfirm:
        {
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_WAITING_FOR_CONFIRM];
            statusInfo = @"任务正等待被确认";
            NSDictionary *userProfile = [[AppUtil sharedInstance].sessionDict objectForKey:KEY_CURRENT_USER_PROFILE];            
            if ([self.workitemInfo objectForKey:@"OwnerId"] != [NSNull null] &&
                [[self.workitemInfo objectForKey:@"OwnerId"] isEqualToString: [userProfile objectForKey:@"Id"]]) {
                [self.workitemActionButton setBackgroundImage: [UIImage imageNamed:@"btn-yellow.png"]
                                                     forState:UIControlStateNormal];                        
                buttonText = @"确认收到";
            } else {
                buttonText = nil;
            }
            break;
        }
        case WorkitemStatusReady:
        {
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_READY];
            [self.workitemActionButton setBackgroundImage: [UIImage imageNamed:@"btn-orange.png"]
                                                 forState:UIControlStateNormal];                        
            statusInfo = @"任务还没有负责人";
            buttonText = @"我来负责";
            break;
        }
    }

    [self.workitemActionButton setTitle:buttonText forState:UIControlStateNormal];
    self.workitemStatusLabel.text = statusInfo;
    self.statusBar.backgroundColor = barColor;    
    
    CGRect headerRect = self.headerContainer.frame;
    if (buttonText != nil) {
        self.workitemActionButton.hidden = NO;  
        headerRect.size.height = HEADER_HEIGHT_WITH_BUTTON;
    } else {
        headerRect.size.height = HEADER_HEIGHT_WITHOUT_BUTTON;                
        self.workitemActionButton.hidden = YES;
    }
    self.headerContainer.frame = headerRect;
    
    [self.tableView setTableHeaderView:self.headerContainer];    
}

- (void) hide
{
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doWorkitemAction:(id)sender {
    WorkitemStatusType currentStatus = (WorkitemStatusType)[[self.workitemInfo objectForKey:@"Status"] intValue];
    if (currentStatus == WorkitemStatusOnGoing || currentStatus == WorkitemStatusExpired) {
        // 完成任务
        [self.service finishWorkitem: [self.workitemInfo objectForKey: @"WorkitemId"]];
    } else if (currentStatus == WorkitemStatusReady) {
        // 我来负责
        [self.service taskOwnershipWorkitem: [self.workitemInfo objectForKey: @"WorkitemId"]];        
    } else if (currentStatus == WorkitemStatusWaitingForConfirm) {
        // 确认收到
        [self.service confirmWorkitem: [self.workitemInfo objectForKey: @"WorkitemId"]];                
    }
    
    [self fetchWorkitemInfoWithWorkitemId:[self.workitemInfo objectForKey: @"WorkitemId"]];
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = barButtonItem;     
    
    
    UIView *modalBackdrop = [[UIView alloc] initWithFrame: [UIScreen mainScreen].bounds];
    modalBackdrop.backgroundColor = [UIColor blackColor];
    modalBackdrop.alpha = 0.0f;
    
    PlanOnDateSelector *planOnDateSelector = [[PlanOnDateSelector alloc] initWithNibName:@"PlanOnDateSelector" bundle:nil];
    planOnDateSelector.modalBackdrop = modalBackdrop;
    planOnDateSelector.mainViewController = self;
    
    self.planOnDateSelector = planOnDateSelector;
    
    if (self.isNotificationMode) {
        [self.navigationController.view addSubview: modalBackdrop];        
        [self.parentViewController.view addSubview: self.planOnDateSelector.view];     
    } else {
        [self.tabBarController.view addSubview: modalBackdrop];        
        [self.tabBarController.view addSubview: self.planOnDateSelector.view];                    
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.isNotificationMode) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action: @selector(hide)];
    }
    if (self.workitemInfo == nil) {
        // 隐藏默认的tableView
//        self.tableView.hidden = YES;
    } else {
        [self setupStatusAndButtons];
        [self.tableView reloadData];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Workitem Description"]) {
        WorkitemDescriptionViewController *workitemDescriptionViewController = segue.destinationViewController;
        workitemDescriptionViewController.workitemInfo = self.workitemInfo;
    } else if ([segue.identifier isEqualToString:@"Edit Workitem Title"]){
        EditWorkitemTitleViewController *editWorkitemTitle = segue.destinationViewController;
        editWorkitemTitle.workitemInfo = self.workitemInfo;
    } else if ([segue.identifier isEqualToString:@"Edit Workitem Project And Tasks"]) {
        EditProjectAndTaskTableViewController *editProjectAndTaskTableViewController = segue.destinationViewController;
        editProjectAndTaskTableViewController.workitemInfo = self.workitemInfo;
        editProjectAndTaskTableViewController.selectedProjectId = [self.workitemInfo valueForKeyPath: @"Project.ProjectId"];
        editProjectAndTaskTableViewController.selectedTaskId = [self.workitemInfo valueForKeyPath: @"RootTask.TaskId"];
    } else if ([segue.identifier isEqualToString:@"Edit Workitem Owners"]) {
        EditWorkitemOwnerViewController *editWorkitemOwnerViewController = segue.destinationViewController;
        editWorkitemOwnerViewController.workitemInfo = self.workitemInfo;
        editWorkitemOwnerViewController.selectedOwnerId = [self.workitemInfo objectForKey:@"OwnerId"];
    }
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_TIME_INFO_INDEX) {
        return @"时间";
    } else if (section == SECTION_DESCRIPTION_INFO_INDEX) {
        return @"备注";
    }
    return nil;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return SECTION_COUNT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_BASIC_INFO_INDEX:
            return SECTION_BASIC_INFO_ROWCOUNT;
        case SECTION_DESCRIPTION_INFO_INDEX:
            return SECTION_DESCRIPTION_INFO_ROWCOUNT;
        case SECTION_TIME_INFO_INDEX:
            return SECTION_TIME_INFO_ROWCOUNT;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_TITLE_INDEX) {
        // Title
        // todo:...
        CellIdentifier = @"Workitem Title Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        
        UILabel *titleLabelView = (UILabel *)[cell viewWithTag:1];
        titleLabelView.text = [self.workitemInfo objectForKey:@"Title"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
        
    }    
    if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_PROJECT_INFO_INDEX) {
        // 项目和RootTask
        CellIdentifier = @"Project Info Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        UILabel *projectTitleLabelView = (UILabel *)[cell viewWithTag: 1];
        projectTitleLabelView.text = [self.workitemInfo valueForKeyPath:@"Project.Title"] == nil ? @"没有相关项目" : [self.workitemInfo valueForKeyPath:@"Project.Title"];
        
        UILabel *sectionTitleLabelView = (UILabel *)[cell viewWithTag: 2];
        sectionTitleLabelView.text = [self.workitemInfo valueForKeyPath:@"RootTask.Title"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
        
    } else if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_OWNER_INFO_INDEX) {
        // 负责人
        CellIdentifier = @"Owner Info Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        UILabel *ownerTitleLabel = (UILabel *)[cell viewWithTag: 1];
        if ([self.workitemInfo objectForKey:@"OwnerName"] != nil) {
            ownerTitleLabel.text = [NSString stringWithFormat:@"%@ 负责", [self.workitemInfo objectForKey:@"OwnerName"]];            
        } else {
            ownerTitleLabel.text = @"未指定";                        
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
        
    } else if (indexPath.section == SECTION_TIME_INFO_INDEX && indexPath.row == ROW_CREATE_ON_INDEX) {
        CellIdentifier = @"CreateOn Cell";        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }                
        UILabel *createOnLabelView = (UILabel *)[cell viewWithTag: 2];
        createOnLabelView.text = [[NSDate dateWithTimeIntervalSince1970:[[self.workitemInfo objectForKey:@"CreateOn"] intValue]] ToFullDateTime];
        return cell;
        
    } else if (indexPath.section == SECTION_TIME_INFO_INDEX && indexPath.row == ROW_PLAN_OR_FINISH_ON_INDEX) {
        CellIdentifier = @"FinishOn Or PlanOn Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }                
        UILabel *titleLabelView = (UILabel *)[cell viewWithTag: 1];        
        UILabel *finishOrPlanOnLabelView = (UILabel *)[cell viewWithTag: 2];
        
        if ([[self.workitemInfo objectForKey:@"Status"] intValue] == WorkitemStatusFinished) {
            titleLabelView.text = @"完成时间";
            finishOrPlanOnLabelView.text = [[NSDate dateWithTimeIntervalSince1970:[[self.workitemInfo objectForKey:@"FinishOn"] intValue]] ToFullDateTime];    
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            titleLabelView.text = @"到期时间";            
            finishOrPlanOnLabelView.text = [self.workitemInfo objectForKey:@"PlanOn"] == [NSNull null] ? @"没有设置" : [[NSDate dateWithTimeIntervalSince1970:[[self.workitemInfo objectForKey:@"PlanOn"] intValue]] ToFullDate];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
        
    } else if (indexPath.section == SECTION_DESCRIPTION_INFO_INDEX && indexPath.row == ROW_DESCRIPTION_INFO_INDEX) {
        CellIdentifier = @"Description Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }                
        if ([self.workitemInfo objectForKey:@"Description"] != [NSNull null] &&
            [[self.workitemInfo objectForKey:@"Description"] length] > 0) {
            cell.textLabel.text = [self.workitemInfo objectForKey:@"Description"];
        } else {
            cell.textLabel.text = @"添加备注";
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    return nil;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSIndexPath *rowToSelect = indexPath;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        rowToSelect = nil;
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    return rowToSelect;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_DESCRIPTION_INFO_INDEX && indexPath.row
        == ROW_DESCRIPTION_INFO_INDEX) {
        if ([self.workitemInfo objectForKey:@"Description"] != [NSNull null] &&
            [[self.workitemInfo objectForKey:@"Description"] length] > 0) {
            return 88.0f;
        } else {
            return 44.0f;
        }        
    };
    return 44.0f;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_DESCRIPTION_INFO_INDEX && indexPath.row == ROW_DESCRIPTION_INFO_INDEX) {
        [self performSegueWithIdentifier:@"Show Workitem Description" sender:self];
    } else if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_TITLE_INDEX) {
        [self performSegueWithIdentifier:@"Edit Workitem Title" sender:self];
    } else if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_PROJECT_INFO_INDEX) {
        [self performSegueWithIdentifier:@"Edit Workitem Project And Tasks" sender:self];
    } else if (indexPath.section == SECTION_BASIC_INFO_INDEX && indexPath.row == ROW_OWNER_INFO_INDEX) {
        [self performSegueWithIdentifier:@"Edit Workitem Owners" sender:self];
    } else if (indexPath.section == SECTION_TIME_INFO_INDEX && indexPath.row == ROW_PLAN_OR_FINISH_ON_INDEX) {
        if ([[self.workitemInfo objectForKey:@"Status"] intValue] != WorkitemStatusFinished) {
            self.planOnDateSelector.workitemInfo = self.workitemInfo;
            [self.planOnDateSelector show];           
            [self.tableView deselectRowAtIndexPath: indexPath animated: YES];
        }
    }
}

- (void)viewDidUnload {
    [self setStatusBar:nil];
    [self setWorkitemStatusLabel:nil];
    [self setWorkitemActionButton:nil];
    [self setHeaderContainer:nil];
    [self setLoadingIndicatorView:nil];
    [super viewDidUnload];
}

# pragma mark -- EzDeskService Delegate

- (void) service:(EzDeskService*)servicevice AccessSuccessWithData: (id) jsonData WithIdentifier: (NSString* ) identifier
{
    if ([identifier isEqualToString:EZ_TAKE_OWNERSHIP_WORKITEM] ||
        [identifier isEqualToString:EZ_FINISH_WORKITEM] ||
        [identifier isEqualToString:EZ_CONFIRM_WORKITEM])
    {
        [self fetchWorkitemInfoWithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]];
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID]) {
        self.navigationItem.rightBarButtonItem = nil;
        //self.tableView.hidden = NO;
        self.workitemInfo = jsonData; 
    }
}
- (void) service:(EzDeskService*)service NetworkError: (NSError*) error WithIdentifier: (NSString* ) identifier
{
    if ([identifier isEqualToString:EZ_TAKE_OWNERSHIP_WORKITEM] ||
        [identifier isEqualToString:EZ_FINISH_WORKITEM] ||
        [identifier isEqualToString:EZ_CONFIRM_WORKITEM])
    {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID]) {
        self.navigationItem.rightBarButtonItem = nil;
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    }
}
- (void) service:(EzDeskService*)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_TAKE_OWNERSHIP_WORKITEM] ||
        [identifier isEqualToString:EZ_FINISH_WORKITEM] ||
        [identifier isEqualToString:EZ_CONFIRM_WORKITEM])
    {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID]) {
        self.navigationItem.rightBarButtonItem = nil;
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    }
}
@end
