//
//  WorkitemTableViewCell.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkitemTableViewCell : UITableViewCell

- (void) setupWorkitemInfo: (NSDictionary *)workitemInfo;

@end
