//
//  EzDeskService.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EzDeskService.h"
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"

#define BUTTON_RETRY_INDEX 1

@interface EzDeskService() <UIAlertViewDelegate>
@end

@implementation EzDeskService
@synthesize delegate = _delegate;

- (UIWindow*) rootWindow {
    return [[UIApplication sharedApplication] delegate].window;
}


- (void) signInWithUsercode: (NSString*)userCode AndPassword: (NSString*)password AndDeviceToken: (NSString *)deviceToken
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc]initWithWindow:[self rootWindow]];
    [[self rootWindow].rootViewController.view addSubview:HUD];
    HUD.labelText = @"登录中...";
    [HUD show:YES];
    
    NSURL *requestUrl = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/auth"];
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetch", NULL);
    dispatch_async(requestQ, ^{
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestUrl];
        [request setPostValue:userCode forKey:@"userName"];
        [request setPostValue:password forKey:@"password"];
        [request setPostValue:deviceToken forKey:@"deviceToken"];        
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{              
            NSError *error = request.error;
            [HUD hide:YES];               
            if (!error){
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];              
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_SIGN_IN_WITH_USERCODE];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceAuthFail
                            AndDescription:@"登录信息无效" 
                            WithIdentifier:EZ_SIGN_IN_WITH_USERCODE];
                }              
            } else {
                UIAlertView *networkAlertView = [[UIAlertView alloc]initWithTitle:@"网络连接出错"
                                                                          message:@"网络连接失败，请检查网络连接。" 
                                                                         delegate:self 
                                                                cancelButtonTitle:@"知道了"
                                                                otherButtonTitles:@"重试", nil];
                [networkAlertView show];                                
            }
        });                                       
    });
    dispatch_release(requestQ);    
}


- (void) checkIfServerAvailable: (NSString*) serverAddress 
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{
        NSString *orignizeInfoUrl = [NSString stringWithFormat:@"http://%@/Core/OrganizationInfo", serverAddress];        
        NSURL *url = [NSURL URLWithString:orignizeInfoUrl];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{        
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData: request.responseData 
                                                                         options: NSJSONReadingAllowFragments 
                                                                           error:&error]; 
                if (!error){
                    [self.delegate service: self 
                     AccessSuccessWithData: jsonData 
                            WithIdentifier: EZ_CHECK_IF_SERVER_AVAILABLE];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_CHECK_IF_SERVER_AVAILABLE];        
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_CHECK_IF_SERVER_AVAILABLE];           
            }
        });
        
    });
    dispatch_release(requestQ);
}

- (void) taskOwnershipWorkitem: (NSString *)workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/DoTakeOwnershipWorkitem"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{               
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&error];                        
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_TAKE_OWNERSHIP_WORKITEM];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_TAKE_OWNERSHIP_WORKITEM];
                } 
            } else {
                [self.delegate service:self 
                          NetworkError:error 
                        WithIdentifier:EZ_TAKE_OWNERSHIP_WORKITEM];           
            }
        });                            
    });
    dispatch_release(requestQ);     
}
- (void) confirmWorkitem: (NSString *)workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/DoConfirmWorkitem"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{           
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];                            
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_CONFIRM_WORKITEM];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_CONFIRM_WORKITEM];
                }  
            } else {
                [self.delegate service:self
                          NetworkError:error
                        WithIdentifier:EZ_CONFIRM_WORKITEM];                
            }
        });
    });
    dispatch_release(requestQ);   
}
- (void) finishWorkitem: (NSString *)workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/DoFinishWorkitem"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: @"1" forKey:@"points"];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{           
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];                            
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_FINISH_WORKITEM];
                    } else {
                        [self.delegate service:self
                                WithAccessFail:EzDeskServiceUpdateDataFail
                                AndDescription:@"更新失败" 
                                WithIdentifier:EZ_FINISH_WORKITEM];
                    }
            } else {
                [self.delegate service:self 
                          NetworkError:error 
                        WithIdentifier:EZ_FINISH_WORKITEM];                
            }
        });                    
    });
    dispatch_release(requestQ);   
}

- (void) addWorkitemWithText: (NSString *)workitemTitle 
                AndProjectId: (NSString *)projectId
                  AndOwnerId: (NSString *)ownerId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/AddWorkitemForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemTitle forKey:@"title"];
        if (projectId.length > 0) {
            [request setPostValue: projectId forKey:@"projectId"];
        }
        if (ownerId.length > 0) {
            [request setPostValue: ownerId forKey:@"ownerId"];
        }
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{           
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];                  
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]) {                
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_ADD_WORKITEM];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_ADD_WORKITEM];
                } 
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_ADD_WORKITEM];             
            }
        });                
    });
    dispatch_release(requestQ);       
}

- (void) deleteWorkitem: (NSString *)workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/DeleteWorkitemForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{              
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];               
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]) {   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_DELETE_WORKITEM];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_DELETE_WORKITEM];
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_DELETE_WORKITEM];                 
            }
        });                
        
    });
    dispatch_release(requestQ);         
}


- (void) updateWorkitemDescription: (NSString *) description 
                    WithWorkitemId: (NSString *)workitemId
{
    // todo: update workitem Info
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/UpdateWorkitemDescriptionForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: description forKey:@"description"];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{          
        NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&error];                       
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_UPDATE_WORKITEM_DESCRIPTION];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_UPDATE_WORKITEM_DESCRIPTION];
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_UPDATE_WORKITEM_DESCRIPTION]; 
            }
        });                  
        
    });
    dispatch_release(requestQ);    
}


- (void) updateWorkitemTitle: (NSString *) title 
              WithWorkitemId: (NSString *) workitemId
{
    // todo: update workitem Info
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/UpdateWorkitemTitleForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: title forKey:@"title"];
        [request startSynchronous];
        dispatch_async(dispatch_get_main_queue(), ^{                      
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData: request.responseData 
                                                                         options: NSJSONReadingAllowFragments 
                                                                           error: &error];         
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_UPDATE_WORKITEM_TITLE];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_UPDATE_WORKITEM_TITLE];
                } 
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_UPDATE_WORKITEM_TITLE]; 
            }            
        });                            
        
    });
    dispatch_release(requestQ);    
}

- (void) updateWorkitemProjectId: (NSString *) projectId 
                       AndTaskId: (NSString *) taskId
                  WithWorkitemId: (NSString *) workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/UpdateWorkitemProjectAndTaskForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: projectId forKey:@"projectId"];
        [request setPostValue: taskId forKey:@"taskId"];        
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{         
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData: request.responseData 
                                                                         options: NSJSONReadingAllowFragments 
                                                                           error: &error];              
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]) {   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_UPDATE_WORKITEM_PROJECT_AND_TASK];
                } else {
                        [self.delegate service:self
                                WithAccessFail:EzDeskServiceUpdateDataFail
                                AndDescription:@"更新失败" 
                                WithIdentifier:EZ_UPDATE_WORKITEM_PROJECT_AND_TASK];
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_UPDATE_WORKITEM_PROJECT_AND_TASK];                    
            }   
        });      
        
    });
    dispatch_release(requestQ);    
}

- (void) updateWorkitemOwner: (NSString *) ownerId 
              WithWorkitemId: (NSString *) workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/UpdateWorkitemOwnerForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: ownerId forKey:@"ownerId"];   
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{              
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];       
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]) {
                        [self.delegate service:self 
                         AccessSuccessWithData:jsonData 
                                WithIdentifier:EZ_UPDATE_WORKITEM_OWNER];
                } else {
                        [self.delegate service:self
                                WithAccessFail:EzDeskServiceUpdateDataFail
                                AndDescription:@"更新失败" 
                                WithIdentifier:EZ_UPDATE_WORKITEM_OWNER];
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_UPDATE_WORKITEM_OWNER];
            } 
        });                   
    });
    dispatch_release(requestQ);    
}

- (void) updateWorkitemPlanOn: (NSString *) planOn 
               WithWorkitemId: (NSString *) workitemId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance] RequestUrlWithFunc:@"/Project/UpdateWorkitemPlanOnForMobile"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue: workitemId forKey:@"workitemId"];
        [request setPostValue: planOn forKey:@"planOn"];   
        [request startSynchronous];

        dispatch_async(dispatch_get_main_queue(), ^{     
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];          
                if (!error && [[jsonData objectForKey:@"Status"] isEqualToString:@"Success"]){   
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_UPDATE_WORKITEM_PLAN_ON];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceUpdateDataFail
                            AndDescription:@"更新失败" 
                            WithIdentifier:EZ_UPDATE_WORKITEM_PLAN_ON];
                } 
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_UPDATE_WORKITEM_PLAN_ON];
            }
        });                        
        
    });
    dispatch_release(requestQ);   
}

- (void) fetchProjectList
{
    // fetch ProjectList 
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc:@"/project/GetAllFullProjectsForMobile"];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{                     
            NSError *error = [request error];            
            if (!error) {
                NSArray* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingAllowFragments error:&error];   
                if (!error) {                      
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_FETCH_PROJECT_LIST];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceAuthFail
                            AndDescription:@"服务器未知错误" 
                            WithIdentifier:EZ_FETCH_PROJECT_LIST];                        
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_PROJECT_LIST];
            }
        });
        
    });
    dispatch_release(requestQ);     
}

- (void) fetchProjectInfoWithProjectId: (NSString *)projectId
{
    // fetch ProjectList 
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc:[NSString stringWithFormat: @"/project/GetWorkitemsInProjectForMobile?projectId=%@", projectId]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{                     
            NSError *error = [request error];            
            if (!error) {
                NSMutableDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingMutableContainers error:&error];  
                if (!error) {                      
                    [self.delegate service:self 
                     AccessSuccessWithData:jsonData 
                            WithIdentifier:EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceAuthFail
                            AndDescription:@"服务器未知错误" 
                            WithIdentifier:EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID];                        
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID];
            }
        });
        
    });
    dispatch_release(requestQ);    
}

- (void) fetchWorkitemInfoWithWorkitemId: (NSString *)workitemId
{
    // fetch workitem Info 
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc:[NSString stringWithFormat: @"/project/GetWorkitemDetailForMobile?workitemId=%@", workitemId]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];

        dispatch_async(dispatch_get_main_queue(), ^{ 
            NSError *error = [request error];
            if (!error) {
                NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                         options:NSJSONReadingAllowFragments 
                                                                           error:&error];  
                if (!error) {                      
                    [self.delegate service: self 
                     AccessSuccessWithData: jsonData 
                            WithIdentifier: EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceAuthFail
                            AndDescription:@"服务器未知错误" 
                            WithIdentifier:EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID];                        
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID];
            }
        });                        
        
    });
    dispatch_release(requestQ);    
}

- (void) fetchMyWorkitemListByWorkitemType: (EzDeskMyWorkitemsType) workitemType
{
    NSArray *fetchWorkitemsUrlArray = [NSArray arrayWithObjects:
                               @"/project/GetMyOwnUnfinishedWorkitemsForMobile",
                               @"/project/GetMyAssignWorkitemsForMobile",
                               @"/project/GetMyOwnFinishedWorkitemsForMobile",                                   
                               nil];
    
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc: [fetchWorkitemsUrlArray objectAtIndex: workitemType]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{           
            NSError *error = [request error];        
            if (!error) {
                NSArray* jsonData = [NSJSONSerialization JSONObjectWithData: request.responseData
                                                                    options: NSJSONReadingAllowFragments 
                                                                      error: &error];            
                if (!error) {                      
                    [self.delegate service: self 
                     AccessSuccessWithData: jsonData 
                            WithIdentifier: EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE];
                } else {
                    [self.delegate service:self
                            WithAccessFail:EzDeskServiceAuthFail
                            AndDescription:@"服务器未知错误" 
                            WithIdentifier:EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE];  
                }               
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE];
            }
        });                 
        
    });
    dispatch_release(requestQ);   
}

- (void)fetchWorkitemOwnerCandidateList
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc: @"/project/GetWorkitemOwnerCandidateForWorkitemForMobile"];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{                     
            NSError *error = [request error];
            if (!error) {
                NSArray* jsonData = [NSJSONSerialization JSONObjectWithData: request.responseData 
                                                                    options: NSJSONReadingAllowFragments 
                                                                      error: &error];  
                if (!error) {                      
                    [self.delegate service: self 
                     AccessSuccessWithData: jsonData 
                            WithIdentifier: EZ_FETCH_WORKITEM_OWNER_CANDIDATE];
                } else {
                    [self.delegate service: self
                            WithAccessFail: EzDeskServiceAuthFail
                            AndDescription: @"服务器未知错误" 
                            WithIdentifier: EZ_FETCH_WORKITEM_OWNER_CANDIDATE];  
                }               
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_WORKITEM_OWNER_CANDIDATE];
            }
        });                              
    });
    dispatch_release(requestQ);   
}

- (void)fetchWorkitemProjectCandidateList
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc: @"/project/GetProjectCadidateForWorkitemForMobile"];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{            
            NSError *error = [request error];
            if (!error) {
                NSArray* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData 
                                                                    options:NSJSONReadingAllowFragments 
                                                                      error:&error];           
                if (!error) {                      
                    [self.delegate service: self 
                     AccessSuccessWithData: jsonData 
                            WithIdentifier: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE];
                } else {
                    [self.delegate service: self
                            WithAccessFail: EzDeskServiceAuthFail
                            AndDescription: @"服务器未知错误" 
                            WithIdentifier: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE];  
                }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE];
            }
        });                            
        
    });
    dispatch_release(requestQ);       
}

- (void)fetchWorkitemRootTaskCandidateListWithProjectId: (NSString *)projectId
{
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetcher", NULL);
    dispatch_async(requestQ, ^{  
        NSURL *url = [[AppUtil sharedInstance]RequestUrlWithFunc: [NSString stringWithFormat: @"/project/GetRootTasksCadidateForWorkitemForMobile?projectId=%@", projectId]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        
        dispatch_async(dispatch_get_main_queue(), ^{     
            NSError *error = [request error];
            if (!error) {
                NSArray* jsonData = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingAllowFragments error:&error];          
                    if (!error) {                      
                        [self.delegate service: self 
                         AccessSuccessWithData: jsonData 
                                WithIdentifier: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE];
                    } else {
                        [self.delegate service: self
                                WithAccessFail: EzDeskServiceAuthFail
                                AndDescription: @"服务器未知错误" 
                                WithIdentifier: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE];                      
                    }
            } else {
                [self.delegate service: self 
                          NetworkError: error 
                        WithIdentifier: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE];
            }
        });                                
        
    });
    dispatch_release(requestQ);       
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == BUTTON_RETRY_INDEX) {
        [self.delegate didNetworkErrorRetryWithIdentifier:EZ_SIGN_IN_WITH_USERCODE];
    }
}
@end
