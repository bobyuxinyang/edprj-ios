//
//  ProjectProgressIndicator.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProjectProgressIndicator.h"
#import "UIColor+WebColor.h"

@implementation ProjectProgressIndicator

@synthesize unfinishedCount = _unfinishedCount, finishedCount = _finishedCount, expiredCount = _expiredCount;

- (id) initWithUnfinishedCount: (NSInteger)unfinishedCount FinishedCount: (NSInteger)finishedCount exipredCount: (NSInteger)expiredCount
{
    self = [super init];
    if (self != nil) {
        self.unfinishedCount = unfinishedCount;
        self.finishedCount = finishedCount;
        self.expiredCount = expiredCount;
    }
    return self;
}

- (void)drawRectangleInContext: (CGContextRef) context WithColor: (UIColor *)color InRect: (CGRect) rect
{
    UIGraphicsPushContext(context);
    CGContextBeginPath(context);    
    CGContextAddRect(context, rect);
    [color setFill];        
    CGContextFillPath(context);
    UIGraphicsPopContext();        
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // draw the light-gray background
    CGSize size = self.bounds.size;
    NSInteger totalCount = self.unfinishedCount + self.finishedCount + self.expiredCount;
    [self drawRectangleInContext:context 
                       WithColor:[UIColor colorWithWebRGB:@"#eeeeee"] 
                          InRect:CGRectMake(0, 0, size.width, size.height)];
    
    if (totalCount > 0) {
        // draw 3 colors
        [self drawRectangleInContext:context 
                           WithColor:[UIColor colorWithWebRGB:@"#999999"] 
                              InRect:CGRectMake(0, 0, size.width, size.height)];
        
        NSInteger finishedHeight = self.finishedCount / (double)totalCount * size.height;
        [self drawRectangleInContext: context 
                           WithColor:[UIColor colorWithWebRGB:@"#3ba365"] 
                              InRect:CGRectMake(0, size.height - finishedHeight, size.width, finishedHeight)];
        
        NSInteger expiredHeight = self.expiredCount / (double)totalCount * size.height;
        [self drawRectangleInContext: context 
                           WithColor:[UIColor colorWithWebRGB:@"#e8465f"]
                              InRect:CGRectMake(0, size.height - finishedHeight - expiredHeight, size.width, expiredHeight)];        
    }
}


@end
