//
//  UIColor+WebColor.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor(WebColor)
+ (UIColor *) colorWithWebRGBA: (NSString *)webColorString;
+ (UIColor *) colorWithWebRGB: (NSString *)webColorString;
@end
