//
//  AppConfig.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConfig : NSObject
- (NSString* )GetValueByKey:(NSString*) key;
+ (AppConfig *) sharedInstance;
@end
