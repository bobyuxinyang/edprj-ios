//
//  AppDelegate.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "WorkitemDetailViewController.h"

@interface AppDelegate() <UIAlertViewDelegate>
@property (nonatomic, strong) NSString *notificationWorkitemId;
@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize notificationWorkitemId = _notificationWorkitemId;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken 
{ 
    [[AppUtil sharedInstance].sessionDict setValue:deviceToken forKey:@"DEVICE_TOKEN"];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err 
{ 
//    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
//    NSLog(@"Device Token Error: %@", str);
}


- (void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ([userInfo objectForKey: @"workitemId"] != nil) {
        if ([[AppUtil sharedInstance].sessionDict objectForKey:KEY_CURRENT_USER_PROFILE] != nil) {
            NSString *msg = [userInfo valueForKeyPath: @"aps.alert"];
            self.notificationWorkitemId = [userInfo objectForKey: @"workitemId"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"易得通知" 
                                                                message:msg 
                                                               delegate:self 
                                                      cancelButtonTitle:@"知道了" otherButtonTitles:@"查看工作任务", nil];
            [alertView show];

        }
    }
}

#pragma mark -- UIAlerView Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && self.notificationWorkitemId != nil) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
           
            WorkitemDetailViewController *workitemDetailViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"Workitem Detail"];
            workitemDetailViewController.isNotificationMode = YES;
            [workitemDetailViewController fetchWorkitemInfoWithWorkitemId: self.notificationWorkitemId];
            
            UINavigationController *navigationControllerForWorkitemDetail = [[UINavigationController alloc] initWithRootViewController: workitemDetailViewController];
            
            UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
            UITabBarController *tabBarController = (UITabBarController*)navigationController.visibleViewController;
            [tabBarController.selectedViewController  presentViewController:navigationControllerForWorkitemDetail animated:YES completion:nil];        

    }
}

@end
