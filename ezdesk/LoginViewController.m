//
//  ViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "AppConfig.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
#import "EzDeskService.h"
#import "EzServerInfo.h"

@interface LoginViewController() <UITextFieldDelegate, EzDeskServiceDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usercodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UILabel *shortNameLabel;

- (void) setupShortName;

@end

@implementation LoginViewController
@synthesize signInButton = _signInButton;
@synthesize usercodeTextField = _usercodeTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize shortNameLabel = _shortNameLabel;
@synthesize service = _service;

#pragma mark -- Properties
-(EzDeskService *)service 
{
    if (!_service) {
        _service = [[EzDeskService alloc]init];
        _service.delegate = self;
    }
    return _service;
}

- (void) setupShortName
{
    NSString* shortName = [[NSUserDefaults standardUserDefaults] objectForKey:ORG_SHORT_NAME];
    if (!shortName) {
        shortName = [[AppConfig sharedInstance] GetValueByKey:ORG_SHORT_NAME];
    }
    self.shortNameLabel.text = shortName;
}


- (IBAction)doLogin:(id)sender {
    NSString* usercode = self.usercodeTextField.text;
    NSString* password = self.passwordTextField.text;
    [self.passwordTextField resignFirstResponder];
    [self.usercodeTextField resignFirstResponder];    

    NSString *deviceToken = [[AppUtil sharedInstance].sessionDict objectForKey:@"DEVICE_TOKEN"];
    
    [self.service signInWithUsercode:usercode AndPassword:password AndDeviceToken:deviceToken];
}
- (IBAction)showSettings:(id)sender {
    [self performSegueWithIdentifier:@"Show Login Settings" sender:self];
}

- (IBAction)showAbout:(id)sender {
    [self performSegueWithIdentifier:@"Show App About" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.usercodeTextField.text = nil;
    self.passwordTextField.text = nil;
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:    UIRemoteNotificationTypeSound
     |UIRemoteNotificationTypeAlert
     |UIRemoteNotificationTypeBadge
     ];
    
    [self setupShortName];
    [self.service checkIfServerAvailable:[AppUtil sharedInstance].serviceAddress];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload {
    [self setShortNameLabel:nil];
    [self setUsercodeTextField:nil];
    [self setPasswordTextField:nil];
    [self setSignInButton:nil];
    [super viewDidUnload];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self doLogin:self];
    return YES;
}


#pragma mark -- EzDeskServiceDelegate
- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_SIGN_IN_WITH_USERCODE]) {
        NSDictionary* userProfile = [jsonData valueForKeyPath:@"Data.UserProfie"];
        [[AppUtil sharedInstance].sessionDict setValue:userProfile forKey:KEY_CURRENT_USER_PROFILE];
        [self performSegueWithIdentifier:@"Sign In Success" sender:self];
    } else if ([identifier isEqualToString:EZ_CHECK_IF_SERVER_AVAILABLE]) {
        EzServerInfo *info = [[EzServerInfo alloc] initWithJsonData: jsonData];                                
        [[NSUserDefaults standardUserDefaults] setValue:info.shortName forKey:ORG_SHORT_NAME];
        [self setupShortName];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_SIGN_IN_WITH_USERCODE]) {    
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"登录出错" 
                                                                 message:@"登录信息无效" 
                                                                delegate:self 
                                                       cancelButtonTitle:@"知道了" 
                                                       otherButtonTitles:nil, nil];
        [errorAlertView show];      
    }
}

- (void)didNetworkErrorRetryWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_SIGN_IN_WITH_USERCODE]) {
        [self doLogin:self];
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_CHECK_IF_SERVER_AVAILABLE]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"网络连接失败" message:@"尝试连接服务器没有成功，你可能无法登录" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil , nil];
        [alertView show];
    }
}


@end
