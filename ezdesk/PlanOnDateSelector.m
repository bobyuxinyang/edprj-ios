//
//  PlanOnDateSelector.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PlanOnDateSelector.h"
#import "EzDeskService.h"
#import "WorkitemDetailViewController.h"

@interface PlanOnDateSelector() <EzDeskServiceDelegate>
@property (strong, nonatomic) EzDeskService *service;
@property (strong, nonatomic) IBOutlet UIDatePicker *planOnDatePicker;
@property (readonly) NSString *selectedDateEpoch;

@end

@implementation PlanOnDateSelector
@synthesize planOnDatePicker = _planOnDatePicker;
@synthesize mainViewController = _mainViewController;
@synthesize modalBackdrop = _modalBackdrop;
@synthesize service = _service;
@synthesize workitemInfo = _workitemInfo;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (NSString *)selectedDateEpoch
{   
    return [NSString stringWithFormat: @"%d", (int)[self.planOnDatePicker.date timeIntervalSince1970]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		//hidden to start
		self.view.frame = CGRectMake(0.0, 480.0, self.view.frame.size.width, self.view.frame.size.height);
        self.view.hidden = YES;
    
        self.planOnDatePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:0]; 
    }
	return self;
}

- (IBAction)cancelButtonClicked:(id)sender {
    [self hide];
}

- (IBAction) doneButtonClicked:(id)sender {
    [self.service updateWorkitemPlanOn: self.selectedDateEpoch
                        WithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]];
	[self hide];
}

- (void) show {
    [UIView animateWithDuration:0.3f animations:^{
        self.view.hidden = NO;        
        
        self.view.frame = CGRectMake(0.0, 
                                     480.0 - self.view.frame.size.height, 
                                     self.view.frame.size.width, 
                                     self.view.frame.size.height);
        self.modalBackdrop.alpha = 0.5;
        self.view.hidden = NO;
        
        if (self.tabBarController != nil) {
            CGRect tabBarFrame = self.tabBarController.tabBar.frame;
            tabBarFrame.origin.y += tabBarFrame.size.height;
            self.tabBarController.tabBar.frame = tabBarFrame;
        }        
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame = CGRectMake(0.0, 
                                     480.0, 
                                     self.view.frame.size.width, 
                                     self.view.frame.size.height);
        self.modalBackdrop.alpha = 0.0;
        self.view.hidden = YES;   
        
        if (self.tabBarController != nil) {
            CGRect tabBarFrame = self.tabBarController.tabBar.frame;
            tabBarFrame.origin.y -= tabBarFrame.size.height;
            self.tabBarController.tabBar.frame = tabBarFrame;    
        }        
    } ];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PLAN_ON]) {    
        WorkitemDetailViewController *workitemDetailViewController =(WorkitemDetailViewController *) self.mainViewController;
        
        NSMutableDictionary *workitemInfo = [NSMutableDictionary dictionaryWithDictionary: self.workitemInfo];    
        [workitemInfo setValue: self.selectedDateEpoch forKey:@"PlanOn"];
        workitemDetailViewController.workitemInfo = workitemInfo;
        
        [self.navigationController popViewControllerAnimated: YES];
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PLAN_ON]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PLAN_ON]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];
    }
}


@end
