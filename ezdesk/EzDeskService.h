//
//  EzDeskService.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EZ_SIGN_IN_WITH_USERCODE @"signInWithUsercode"
#define EZ_CHECK_IF_SERVER_AVAILABLE @"checkIfServerAvailable"
#define EZ_TAKE_OWNERSHIP_WORKITEM @"takeOwnershipWorkitem"
#define EZ_CONFIRM_WORKITEM @"confirmWorkitem"
#define EZ_FINISH_WORKITEM @"finisheWorkitem"
#define EZ_ADD_WORKITEM @"addWorkitem"
#define EZ_DELETE_WORKITEM @"deleteWorkite"
#define EZ_UPDATE_WORKITEM_DESCRIPTION @"updateWorkitemDescription"
#define EZ_UPDATE_WORKITEM_TITLE @"updateWorkitemTitle"
#define EZ_UPDATE_WORKITEM_OWNER @"updateWorkitemOwner"
#define EZ_UPDATE_WORKITEM_PROJECT_AND_TASK @"updateWorkitemProjectAndTask"
#define EZ_UPDATE_WORKITEM_PLAN_ON @"updateWorkitemPlanOn"
#define EZ_FETCH_PROJECT_LIST @"fetchProjectList"
#define EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID @"fetchProjectInfoWithProjectId"
#define EZ_FETCH_WORKITEMINFO_WITH_WORKITEM_ID @"fetchWorkitemInfoWithWorkitemId"
#define EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE @"fetchMyWorkitemListByWorkitemType"
#define EZ_FETCH_WORKITEM_OWNER_CANDIDATE @"fetchWorkitemOwnerCandidate"
#define EZ_FETCH_WORKITEM_PROJECT_CANDIDATE @"fetchWorkitemProjectCandidate"
#define EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE @"fetchWorkitemRootTasksCandidate"

typedef enum{
    EzDeskServiceAuthFail,
    EzDeskServiceUpdateDataFail
} EzDeskAccessFailType;

typedef enum {
    EzDeskTypeMyOwnUnfinishedWorkitems = 0,
    EzDeskTypeMyAssignWorkitems = 1,
    EzDeskTypeMyOwnFinishedWorkitems = 2,    
} EzDeskMyWorkitemsType;

@class EzDeskService;

@protocol EzDeskServiceDelegate <NSObject>

@optional
- (void) didNetworkErrorRetryWithIdentifier: (NSString* ) identifier;
- (void) service:(EzDeskService*)service WithAccessFail:(EzDeskAccessFailType) failType AndDescription:(NSString*)description WithIdentifier: (NSString* ) identifier;
- (void) service:(EzDeskService*)service AccessSuccessWithData: (id) jsonData WithIdentifier: (NSString* ) identifier;
- (void) service:(EzDeskService*)service NetworkError: (NSError*) error WithIdentifier: (NSString* ) identifier;

@end

@interface EzDeskService : NSObject

@property (nonatomic, weak) id<EzDeskServiceDelegate> delegate;

- (void)signInWithUsercode: (NSString*)userCode AndPassword: (NSString*)password AndDeviceToken: (NSString *)deviceToken;
- (void)checkIfServerAvailable: (NSString*) serverAddress;

- (void)taskOwnershipWorkitem: (NSString *)workitemId;
- (void)confirmWorkitem: (NSString *)workitemId;
- (void)finishWorkitem: (NSString *)workitemId;
- (void)addWorkitemWithText: (NSString *)workitemTitle 
               AndProjectId: (NSString *)projectId
                 AndOwnerId: (NSString *)ownerId;
- (void)deleteWorkitem: (NSString *)workitemId;

- (void)updateWorkitemDescription: (NSString *) description 
                   WithWorkitemId: (NSString *)workitemId;
- (void)updateWorkitemTitle: (NSString *) title 
             WithWorkitemId: (NSString *) workitemId;
- (void)updateWorkitemProjectId: (NSString *) projectId 
                      AndTaskId: (NSString *) taskId
                 WithWorkitemId: (NSString *) workitemId;
- (void)updateWorkitemOwner: (NSString *) ownerId 
                 WithWorkitemId: (NSString *) workitemId;
- (void)updateWorkitemPlanOn: (NSString *) planOn 
             WithWorkitemId: (NSString *) workitemId;

- (void)fetchProjectList;
- (void)fetchProjectInfoWithProjectId: (NSString *)projectId;
- (void)fetchWorkitemInfoWithWorkitemId: (NSString *)workitemId;
- (void)fetchMyWorkitemListByWorkitemType: (EzDeskMyWorkitemsType) workitemType;
- (void)fetchWorkitemOwnerCandidateList;
- (void)fetchWorkitemProjectCandidateList;
- (void)fetchWorkitemRootTaskCandidateListWithProjectId: (NSString *)projectId;

@end
