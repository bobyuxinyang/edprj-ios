#define SERVER_ADDRESS @"ServerAddress"
#define ORG_SHORT_NAME @"ShortName"

#define KEY_CURRENT_USER_PROFILE @"CurrentUserProfile"

typedef enum{
    WorkitemStatusOnGoing = 0, // 蓝色正在进行中    
    WorkitemStatusFinished = 1, // 绿色完成
    WorkitemStatusExpired = 2, // 红色过期
    WorkitemStatusWaitingForConfirm = 3, // 红色过期        
    WorkitemStatusReady = 4,  // 灰色没有负责人等待就绪
} WorkitemStatusType;

#define COLOR_FOR_WORKITEM_STATUS_ON_GOING @"#65A5F2"
#define COLOR_FOR_WORKITEM_STATUS_FINISHED @"#3BA365"
#define COLOR_FOR_WORKITEM_STATUS_EXPIRED @"#FF6347"
#define COLOR_FOR_WORKITEM_STATUS_WAITING_FOR_CONFIRM @"#FFD700"
#define COLOR_FOR_WORKITEM_STATUS_READY @"#999999"

