//
//  AppUtil.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppUtil.h"
#import "AppConfig.h"
#import "ASIHTTPRequest.h"

@interface AppUtil() <UIAlertViewDelegate>

@end

@implementation AppUtil
@synthesize sessionDict = _sessionDict;

- (NSMutableDictionary*) sessionDict
{
    @synchronized(self) {
        if (!_sessionDict) {
            _sessionDict = [[NSMutableDictionary alloc] init];
        }
    }
    return _sessionDict;
}

static AppUtil *_sharedInstance = nil;
+ (AppUtil *) sharedInstance
{
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[self alloc] init];
        }
    }    
    return _sharedInstance;
}

- (NSString *)serviceAddress
{
    NSString* currentServerAddress = [[NSUserDefaults standardUserDefaults] objectForKey:SERVER_ADDRESS];
    if (currentServerAddress == nil) {
        currentServerAddress = [[AppConfig sharedInstance] GetValueByKey:SERVER_ADDRESS];
    }
    return currentServerAddress;
}

- (void)setServiceAddress:(NSString *)serviceAddress
{
    [[NSUserDefaults standardUserDefaults] setValue:serviceAddress forKey:SERVER_ADDRESS];
}

- (NSURL*)RequestUrlWithFunc: (NSString*) funcUrl
{
    //@"/auth"
    NSString *currentServerAddress = self.serviceAddress;
    if (!currentServerAddress) {
        currentServerAddress = [[AppConfig sharedInstance] GetValueByKey:SERVER_ADDRESS];
    }
    return [[NSURL alloc] initWithString: [NSString stringWithFormat:@"http://%@%@", currentServerAddress, funcUrl]];
}

- (void)showNetworkErrorAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"网络连接失败" 
                                                        message: @"目前网络连接无效。" 
                                                       delegate:self 
                                              cancelButtonTitle:@"知道了" 
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)showAccessFailErrorAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"服务器异常" 
                                                        message: @"很抱歉，可能因为服务器正在升级维护，服务器目前状态异常。请尝试退出重新登录。" 
                                                       delegate:self 
                                              cancelButtonTitle:@"知道了" 
                                              otherButtonTitles:@"重新登录", nil];
    [alertView show];
}

- (void)signOutApplication
{
    [ASIHTTPRequest setSessionCookies:nil];
    [[AppUtil sharedInstance].sessionDict removeAllObjects];
    [[UIApplication sharedApplication].keyWindow.rootViewController dismissModalViewControllerAnimated: YES];
}


#pragma mark -- UIAlertView

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self signOutApplication];
    }
}

@end
