//
//  EzServerInfo.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EzServerInfo.h"

@implementation EzServerInfo
@synthesize name = name, shortName = _shortName;

- (id)initWithJsonData: (NSDictionary*)jsonData
{
    if (!self) {
        self = [[EzServerInfo alloc]init];
    }
    self.name = [jsonData objectForKey:@"FullName"];
    self.shortName = [jsonData objectForKey:@"ShortName"];
    return self;
}
@end
