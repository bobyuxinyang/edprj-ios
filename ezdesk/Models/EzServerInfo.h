//
//  EzServerInfo.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EzServerInfo : NSObject
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* shortName;

- (id)initWithJsonData: (NSDictionary*)jsonData;
@end
