//
//  EditProjectAndTaskTableViewController.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProjectAndTaskTableViewController : UITableViewController
@property (nonatomic, strong) NSDictionary *workitemInfo;
@property (nonatomic, strong) NSString* selectedProjectId;
@property (nonatomic, strong) NSString* selectedTaskId;

@end
