//
//  UIColor+WebColor.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIColor+WebColor.h"

@implementation UIColor(WebColor)

+ (UIColor *) colorWithWebRGBA: (NSString *)webColorString
{
    
    NSString *redValueStr = [webColorString substringWithRange:NSMakeRange(1, 2)];
    NSString *greenValueStr = [webColorString substringWithRange:NSMakeRange(3, 2)];
    NSString *blueValueStr = [webColorString substringWithRange:NSMakeRange(5, 2)];    
    NSString *alphaValueStr = [webColorString substringWithRange:NSMakeRange(7, 2)];        
    unsigned long redValue = strtoul([redValueStr UTF8String], 0, 16);
    unsigned long greenValue = strtoul([greenValueStr UTF8String], 0, 16);
    unsigned long blueValue = strtoul([blueValueStr UTF8String], 0, 16);
    unsigned long alphaValue = strtoul([alphaValueStr UTF8String], 0, 16);    
    
    return [UIColor colorWithRed:redValue / 255.0f green:greenValue / 255.0f blue:blueValue / 255.0f alpha:alphaValue / 255.0f];    
}

+ (UIColor *) colorWithWebRGB: (NSString *)webColorString
{
    
    NSString *redValueStr = [webColorString substringWithRange:NSMakeRange(1, 2)];
    NSString *greenValueStr = [webColorString substringWithRange:NSMakeRange(3, 2)];
    NSString *blueValueStr = [webColorString substringWithRange:NSMakeRange(5, 2)];      
    unsigned long redValue = strtoul([redValueStr UTF8String], 0, 16);
    unsigned long greenValue = strtoul([greenValueStr UTF8String], 0, 16);
    unsigned long blueValue = strtoul([blueValueStr UTF8String], 0, 16);
    
    return [UIColor colorWithRed:redValue / 255.0f green:greenValue / 255.0f blue:blueValue / 255.0f alpha:1];    
}

@end