//
//  WorkitemTableViewCell.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WorkitemTableViewCell.h"
#import "WorkitemStatusIndicator.h"
#import <QuartzCore/QuartzCore.h>

@implementation WorkitemTableViewCell

- (void) setupWorkitemInfo: (NSDictionary *)workitemInfo
{
    self.textLabel.text = [workitemInfo objectForKey:@"Title"];
    
    WorkitemStatusIndicator *indicator = [[WorkitemStatusIndicator alloc] initIndicatorWithStatus: [[workitemInfo objectForKey:@"Status"] intValue]];
    indicator.frame = CGRectMake(0.0f, 0.0f, 6.0f, self.bounds.size.height - 1);
    
    [self addSubview:indicator];
    
}

@end
