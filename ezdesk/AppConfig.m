//
//  AppConfig.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppConfig.h"

#define APPCONFIG_FILENAME @"appconfig"

@interface AppConfig()
@property (nonatomic, strong, readonly) NSDictionary *configDictionary;
@end


@implementation AppConfig

@synthesize configDictionary = _configDictionary;

static AppConfig *_sharedInstance = nil;
+ (AppConfig *) sharedInstance
{
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[self alloc] init];
        }
    }    
    return _sharedInstance;
}

-(NSDictionary*)configDictionary
{
    if (_configDictionary == nil) {

        _configDictionary = [[NSDictionary alloc] initWithContentsOfFile: 
                             [[NSBundle mainBundle] pathForResource:APPCONFIG_FILENAME ofType:@"plist"]
                             ];
    }
    return _configDictionary;
}

- (NSString* )GetValueByKey:(NSString*) key
{
    return [self.configDictionary objectForKey:key];
}


@end
