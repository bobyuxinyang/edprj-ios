//
//  WorkitemDescriptionViewController.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EzDeskService.h"

@interface WorkitemDescriptionViewController : UIViewController <EzDeskServiceDelegate, UITextViewDelegate>
@property (nonatomic, strong) NSDictionary *workitemInfo;
@end
