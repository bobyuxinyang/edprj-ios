//
//  MyOwnUnfinishedWorkitemsTableViewController.h
//  ezdesk
//
//  Created by Bruce Yang on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EzDeskService.h"

@interface MyWorkitemsTableViewController : UITableViewController
@property (nonatomic, strong) NSString *selectedWorkitemId;
@property (nonatomic) EzDeskMyWorkitemsType workitemType;
@end
