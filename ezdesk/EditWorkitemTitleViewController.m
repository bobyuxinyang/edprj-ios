//
//  EditWorkitemTitleViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EditWorkitemTitleViewController.h"
#import "EzDeskService.h"
#import "WorkitemDetailViewController.h"

@interface EditWorkitemTitleViewController() <EzDeskServiceDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *workitemTitleTextField;
@property (strong, nonatomic) EzDeskService *service;

@end

@implementation EditWorkitemTitleViewController
@synthesize workitemTitleTextField = _workitemTitleTextField;
@synthesize workitemInfo = _workitemInfo;
@synthesize service = _service;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}


- (IBAction)editWorkitemTitle:(id)sender {
    NSString *newTitle = self.workitemTitleTextField.text;
    NSMutableDictionary *workitemInfo = [NSMutableDictionary dictionaryWithDictionary: self.workitemInfo];
    [workitemInfo setValue: newTitle forKey:@"Title"];
    self.workitemInfo = workitemInfo;
    
    [self.service updateWorkitemTitle: newTitle 
                       WithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.workitemTitleTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.workitemTitleTextField.text = [self.workitemInfo objectForKey:@"Title"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload {
    [self setWorkitemTitleTextField:nil];
    [super viewDidUnload];
}


#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_UPDATE_WORKITEM_TITLE]) {
        int count = [self.navigationController.viewControllers count];
        WorkitemDetailViewController *workitemDetailViewController = [self.navigationController.viewControllers objectAtIndex:count - 2];
        workitemDetailViewController.workitemInfo = self.workitemInfo;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_TITLE]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        [self.navigationController popViewControllerAnimated:YES];        
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_TITLE]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        [self.navigationController popViewControllerAnimated:YES];
    }  
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self editWorkitemTitle: self];
    return YES;
}
@end
