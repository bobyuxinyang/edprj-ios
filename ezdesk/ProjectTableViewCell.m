//
//  ProjectTableViewCell.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProjectTableViewCell.h"
#import "ProjectProgressIndicator.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProjectTableViewCell

@synthesize textLabel, detailTextLabel;

-(void)setupProjectInfo:(NSDictionary*)projectInfo
{
    self.textLabel.text = [projectInfo objectForKey:@"Name"];
    self.detailTextLabel.text = [projectInfo objectForKey:@"Explanation"];
    
    //"Workitem":{"UnfinishedCount":16,"FinishedCount":3,"ExpiredCount":0},
    NSInteger unfinishedCount = [[projectInfo valueForKeyPath:@"Workitem.UnfinishedCount"] intValue];
    NSInteger finishedCount = [[projectInfo valueForKeyPath:@"Workitem.FinishedCount"] intValue];
    NSInteger expiredCount = [[projectInfo valueForKeyPath:@"Workitem.ExpiredCount"] intValue];    
    
    ProjectProgressIndicator *indicator = [[ProjectProgressIndicator alloc] initWithUnfinishedCount:unfinishedCount FinishedCount:finishedCount exipredCount:expiredCount];
    indicator.frame = CGRectMake(0.0f, 0.0f, 6.0f, self.bounds.size.height - 1);
    

    [self addSubview:indicator];
}

@end
