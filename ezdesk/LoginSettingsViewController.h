//
//  LoginSettingsViewController.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EzDeskService.h"

@interface LoginSettingsViewController : UIViewController
@property (nonatomic, strong, readonly) EzDeskService *service;
@end
