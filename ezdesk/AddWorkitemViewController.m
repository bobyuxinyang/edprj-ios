//
//  AddWorkitemViewController.m
//  ezdesk
//
//  Created by Bruce Yang on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddWorkitemViewController.h"
#import "EzDeskService.h"
#import "ProjectTasksViewController.h"
#import "MyWorkitemsTableViewController.h"

@interface AddWorkitemViewController() <EzDeskServiceDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *workitemNameTextFieldView;

@property (nonatomic, strong) EzDeskService *service;

@end

@implementation AddWorkitemViewController
@synthesize workitemNameTextFieldView = _workitemNameTextFieldView;
@synthesize projectId = _projectId, isOwner = _isOwner;
@synthesize service = _service;
@synthesize sourceController = _sourceController;
@synthesize sourceType = _sourceType;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)completeAddWorkitem:(id)sender {
    if (self.workitemNameTextFieldView.text.length > 0) {
        NSString *ownerId;
        if (self.isOwner) {
            ownerId = [[[AppUtil sharedInstance].sessionDict objectForKey:KEY_CURRENT_USER_PROFILE] objectForKey:@"Id"];
        }
        // do add workitem
        [self.service addWorkitemWithText: self.workitemNameTextFieldView.text 
                             AndProjectId: self.projectId
                               AndOwnerId: ownerId];
    } else {
        // go back
        [self.presentingViewController dismissModalViewControllerAnimated:YES];
    }
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [self.workitemNameTextFieldView becomeFirstResponder];
    if (self.sourceType == EzDeskMyAssignWorkitemsSourceType) {
        self.navigationItem.title = @"布置任务";
    } else if (self.sourceType == EzDeskMyUnfinishedWorkitemsSourceType){
        self.navigationItem.title = @"添加我的任务";
    } else if (self.sourceType == EzDeskProjectTasksSourceType) {
        self.navigationItem.title = @"添加项目任务";
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.workitemNameTextFieldView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setWorkitemNameTextFieldView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -- EzDesk Service Delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:EZ_ADD_WORKITEM]) {
        
        [self.presentingViewController dismissModalViewControllerAnimated:YES];
        NSString *workitemId = [jsonData valueForKeyPath:@"Data.workitemId"];
        
        if (self.sourceType == EzDeskProjectTasksSourceType){
            ProjectTasksViewController *projectTasksViewController = (ProjectTasksViewController *)self.sourceController;
            projectTasksViewController.selectedWorkitemId = workitemId;
         
            [projectTasksViewController performSegueWithIdentifier:@"Show Project Workitem Detail" sender:projectTasksViewController];            
        } else if (self.sourceType == EzDeskMyUnfinishedWorkitemsSourceType || self.sourceType == EzDeskMyAssignWorkitemsSourceType) {
            MyWorkitemsTableViewController *myOwnUnfinishedWorkitemTableViewController = (MyWorkitemsTableViewController *)self.sourceController;
            
            myOwnUnfinishedWorkitemTableViewController.selectedWorkitemId = workitemId;
            [myOwnUnfinishedWorkitemTableViewController performSegueWithIdentifier:@"Show Workitem Detail" sender:myOwnUnfinishedWorkitemTableViewController];
        }
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_ADD_WORKITEM]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_ADD_WORKITEM]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];        
    }
}

#pragma mark -- UITextFileld Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self completeAddWorkitem:self];
    return YES;
}

@end
