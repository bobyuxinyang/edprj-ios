//
//  MyOwnUnfinishedWorkitemsTableViewController.m
//  ezdesk
//
//  Created by Bruce Yang on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyWorkitemsTableViewController.h"
#import "ASIHTTPRequest.h"
#import "WorkitemDetailViewController.h"
#import "AddWorkitemViewController.h"
#import "EzDeskService.h"
#import "WorkitemTableViewCell.h"
#import "EzDeskService.h"

@interface MyWorkitemsTableViewController() <EzDeskServiceDelegate>
@property (strong, nonatomic) IBOutlet UISegmentedControl *workitemTypeSelector;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addWorkitemButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;
@property (strong, nonatomic) EzDeskService *service;
@property (nonatomic, strong) NSMutableArray *workitems;
@property (nonatomic, strong) NSArray *workitemsPageTitleArray;
- (void)fetchMyWorkitemListByWorkitemType;
@end

@implementation MyWorkitemsTableViewController
@synthesize workitemTypeSelector = _workitemTypeSelector;
@synthesize workitems = _workitems;
@synthesize selectedWorkitemId = _selectedWorkitemId;
@synthesize workitemType = _workitemType;
@synthesize workitemsPageTitleArray = _workitemsPageTitleArray;
@synthesize addWorkitemButton = _addWorkitemButton;
@synthesize loadingIndicatorView = _loadingIndicatorView;
@synthesize service = _service;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)setWorkitemType: (EzDeskMyWorkitemsType)workitemType
{
    if (workitemType != _workitemType) {
        _workitemType = workitemType;
        [self fetchMyWorkitemListByWorkitemType];        
    }
}

- (void)setWorkitems:(NSMutableArray *)workitems
{
    if (![workitems isEqual:_workitems]) {
        _workitems = workitems;
        
        [self.tableView reloadData];
    }
}

- (void)fetchMyWorkitemListByWorkitemType
{
    [self.loadingIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.service fetchMyWorkitemListByWorkitemType: self.workitemType];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) configCell: (WorkitemTableViewCell *)cell 
      WithIndexPath: (NSIndexPath *) indexPath
{
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSDictionary *workitem = [self.workitems objectAtIndex:indexPath.row];
    [cell setupWorkitemInfo: workitem];    
}

- (IBAction)addWorkitem:(id)sender {
    [self performSegueWithIdentifier:@"Add Workitem" sender:self];
}


- (IBAction)changeWorkitemType:(id)sender {
    self.workitemType = self.workitemTypeSelector.selectedSegmentIndex;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    _workitemType = EzDeskTypeMyOwnUnfinishedWorkitems;    

    _workitemsPageTitleArray = [NSArray arrayWithObjects:
                                @"我负责的任务",
                                @"我布置的任务",
                                @"我完成的任务", nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchMyWorkitemListByWorkitemType];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Workitem Detail"]) {
        WorkitemDetailViewController *workitemDetailViewController = segue.destinationViewController;
        [workitemDetailViewController fetchWorkitemInfoWithWorkitemId: self.selectedWorkitemId];
    } else if([segue.identifier isEqualToString:@"Add Workitem"]){
        UINavigationController *navigationController = segue.destinationViewController;
        AddWorkitemViewController *addWorkitemController = [navigationController.viewControllers lastObject];
        addWorkitemController.sourceController = self;
        if (self.workitemType == EzDeskTypeMyOwnUnfinishedWorkitems) {
            addWorkitemController.isOwner = YES;
            addWorkitemController.sourceType = EzDeskMyUnfinishedWorkitemsSourceType;
        } else if (self.workitemType == EzDeskTypeMyAssignWorkitems){
            addWorkitemController.isOwner = NO;
            addWorkitemController.sourceType = EzDeskMyAssignWorkitemsSourceType;
        }
    }
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.workitems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Workitem Cell";
    
    WorkitemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[WorkitemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configCell: cell WithIndexPath: indexPath];
    
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSString *workitemId = [[self.workitems objectAtIndex: indexPath.row] objectForKey:@"WorkitemId"];
        [self.service deleteWorkitem: workitemId];
        [self.workitems removeObjectAtIndex:indexPath.row];        
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *workitem = [self.workitems objectAtIndex:indexPath.row];
    self.selectedWorkitemId = [workitem objectForKey:@"WorkitemId"];
    [self performSegueWithIdentifier:@"Show Workitem Detail" sender:self];
}

- (void)viewDidUnload {
    [self setWorkitemTypeSelector:nil];
    [self setAddWorkitemButton:nil];
    [self setAddWorkitemButton:nil];
    [self setLoadingIndicatorView:nil];
    [super viewDidUnload];
}

#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE]) {
        self.navigationItem.title = [self.workitemsPageTitleArray objectAtIndex:self.workitemType];
        if (self.workitemType == EzDeskTypeMyOwnFinishedWorkitems) {    
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        }        
        self.workitems = [[NSMutableArray alloc] initWithArray: jsonData];
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        if (self.workitemType == EzDeskTypeMyOwnFinishedWorkitems) {    
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        }
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_MYWORKITEMLIST_BY_WORKITEM_TYPE]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        if (self.workitemType == EzDeskTypeMyOwnFinishedWorkitems) {    
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        }
    }
}
@end
