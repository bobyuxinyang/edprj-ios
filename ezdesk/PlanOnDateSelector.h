//
//  PlanOnDateSelector.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanOnDateSelector : UIViewController
@property (nonatomic, strong) NSDictionary *workitemInfo;
@property (nonatomic, strong) UIViewController *mainViewController;
@property (nonatomic, strong) UIView *modalBackdrop;
- (void)show;
- (void)hide;
@end
