//
//  WorkitemStatusIndicator.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProjectProgressIndicator.h"


@interface WorkitemStatusIndicator: UIView

- (id)initIndicatorWithStatus: (WorkitemStatusType) workitemStatusType;

@end
