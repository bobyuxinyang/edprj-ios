//
//  WorkitemDescriptionViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WorkitemDescriptionViewController.h"
#import "EzDeskService.h"
#import "WorkitemDetailViewController.h"

@interface WorkitemDescriptionViewController()
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) EzDeskService *service;
@end

@implementation WorkitemDescriptionViewController
@synthesize descriptionTextView = _descriptionTextView;

@synthesize workitemInfo = _workitemInfo;
@synthesize service = _service;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = [self.workitemInfo objectForKey:@"Title"];
    if ([self.workitemInfo objectForKey:@"Description"] != [NSNull null]) {
        self.descriptionTextView.text = [self.workitemInfo objectForKey:@"Description"];
    }

    self.editing = YES;
}

- (void)viewDidUnload
{
    [self setDescriptionTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    self.descriptionTextView.editable = editing;
    self.navigationItem.hidesBackButton = editing;
    if (editing) {
        [self.descriptionTextView becomeFirstResponder];
    } else {
        NSMutableDictionary *workitemInfo = [[NSMutableDictionary alloc] initWithDictionary:self.workitemInfo];
        [workitemInfo setValue:self.descriptionTextView.text forKey:@"Description"];
        self.workitemInfo = workitemInfo;

        [self.service updateWorkitemDescription: [self.workitemInfo objectForKey:@"Description"]
                                 WithWorkitemId: [self.workitemInfo objectForKey:@"WorkitemId"]];
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self setEditing:NO animated:YES];
    return YES;
}

# pragma mark -- EzDeskService Delegate


- (void) service:(EzDeskService*)servicevice AccessSuccessWithData: (id) jsonData WithIdentifier: (NSString* ) identifier
{
    if ([identifier isEqualToString:EZ_UPDATE_WORKITEM_DESCRIPTION]) {
        int count = [self.navigationController.viewControllers count];
        WorkitemDetailViewController *workitemDetailViewController = [self.navigationController.viewControllers objectAtIndex:count - 2];
        workitemDetailViewController.workitemInfo = self.workitemInfo;
        [self.navigationController popViewControllerAnimated: YES];
    }
}
- (void) service:(EzDeskService*)service NetworkError: (NSError*) error WithIdentifier: (NSString* ) identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_DESCRIPTION]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];  
        [self.navigationController popViewControllerAnimated: YES];        
    }
}
- (void) service:(EzDeskService*)service WithAccessFail:(EzDeskAccessFailType) failType AndDescription:(NSString*)description WithIdentifier: (NSString* ) identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_DESCRIPTION]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];    
    }
}

@end
