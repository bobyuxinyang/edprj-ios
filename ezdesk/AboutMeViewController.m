//
//  AboutMeViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AboutMeViewController.h"
#import "AppConfig.h"
#import "ASIHTTPRequest.h"

@interface AboutMeViewController() 
@property (weak, nonatomic) IBOutlet UITextField *nameAndDeptLabel;
@property (weak, nonatomic) IBOutlet UITextField *positionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userHeadImage;

- (void) setupUserInfo;
@end

@implementation AboutMeViewController
@synthesize nameAndDeptLabel;
@synthesize positionLabel;
@synthesize userHeadImage;

- (void) setupUserInfo
{
    NSDictionary* userProfile = [[AppUtil sharedInstance].sessionDict objectForKey:KEY_CURRENT_USER_PROFILE];    
    self.nameAndDeptLabel.text = [NSString stringWithFormat:@"%@（%@）", 
                                  [userProfile objectForKey:@"Name"],
                                  [userProfile valueForKeyPath:@"Dept.DeptName"]];    
    self.positionLabel.text = [userProfile valueForKeyPath:@"Position.Description"];
    self.navigationItem.title = [[NSUserDefaults standardUserDefaults] objectForKey:ORG_SHORT_NAME];    
    
    // download head image
    dispatch_queue_t requestQ = dispatch_queue_create("EzDesk fetech", NULL);
    dispatch_async(requestQ, ^{
        
        NSURL *imageUrl = [[AppUtil sharedInstance] RequestUrlWithFunc: 
                           [NSString stringWithFormat: @"/core/headicon/%@", [userProfile objectForKey: @"Id"]
                            ]];
        NSData *imageData = [[NSData alloc] initWithContentsOfURL: imageUrl];    
        dispatch_async(dispatch_get_main_queue(), ^{            
            self.userHeadImage.image = [UIImage imageWithData:imageData];   
        });
    });
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupUserInfo];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        [[AppUtil sharedInstance] signOutApplication];
    }
}

- (void)viewDidUnload {
    [self setNameAndDeptLabel:nil];
    [self setPositionLabel:nil];
    [self setUserHeadImage:nil];
    [super viewDidUnload];
}
@end
