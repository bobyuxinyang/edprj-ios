//
//  ProjectTasksViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProjectTasksViewController.h"
#import "EzDeskService.h"
#import "WorkitemTableViewCell.h"
#import "WorkitemDetailViewController.h"
#import "AddWorkitemViewController.h"
#import "EzDeskService.h"

@interface ProjectTasksViewController() <EzDeskServiceDelegate>
@property (nonatomic, strong) NSMutableDictionary *projectInfo;
@property (nonatomic, strong) EzDeskService *service;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addWorkitemButton;
- (void)fetchProjectInfoWithProjectId: (NSString *)projectId;
@end


@implementation ProjectTasksViewController
@synthesize projectTaskTableview = _projectTaskTableview;
@synthesize projectInfo = _projectInfo;
@synthesize selectedWorkitemId = _selectedWorkitemId;
@synthesize projectId = _projectId;
@synthesize service = _service;
@synthesize loadingIndicatorView = _loadingIndicatorView;
@synthesize addWorkitemButton = _addWorkitemButton;
@synthesize projectTitle = _projectTitle;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)setProjectInfo:(NSMutableDictionary *)projectInfo
{
    if (![projectInfo isEqual:_projectInfo]) {
        _projectInfo = projectInfo;
        [self.tableView reloadData];
        self.navigationItem.title = [projectInfo valueForKeyPath:@"ProjectInfo.Title"];
    }
}

- (void)setProjectTitle:(NSString *)projectTitle
{
    self.navigationItem.title = projectTitle;
}

- (NSDictionary *)getWorkitemFromProjectInfoWithIndexPath: (NSIndexPath *)indexPath
{
    NSArray *rootTasks = [self.projectInfo objectForKey:@"RootTasks"];
    int skipCount = 0;
    for (int i = 0; i < indexPath.section; i++){
        skipCount += [[(NSDictionary *)[rootTasks objectAtIndex:i] objectForKey:@"WorkitemCount"] intValue];
    }
    skipCount += indexPath.row;
    NSDictionary *workitem = [((NSArray *)[self.projectInfo objectForKey:@"SortedWorkitems"]) objectAtIndex:skipCount];
    return workitem;
}

- (void) removeWorkitemInProjectAtIndexPath: (NSIndexPath *)indexPath
{
    NSArray *rootTasks = [self.projectInfo objectForKey:@"RootTasks"];
    if (indexPath.section < [rootTasks count]) {
        NSDictionary *rootTask = [rootTasks objectAtIndex: indexPath.section];
        NSInteger workitemCount = [[rootTask objectForKey: @"WorkitemCount"] intValue];
        workitemCount --;
        [rootTask setValue: [NSString stringWithFormat:@"%d", workitemCount ] forKey: @"WorkitemCount"];
    }
    int skipCount = 0;
    for (int i = 0; i < indexPath.section; i++){
        skipCount += [[(NSDictionary *)[rootTasks objectAtIndex:i] objectForKey:@"WorkitemCount"] intValue];
    }
    skipCount += indexPath.row;
    
    NSMutableArray *mutableArray = [self.projectInfo objectForKey:@"SortedWorkitems"];

    NSDictionary *workitem = [mutableArray objectAtIndex:skipCount];    
    NSString *workitemId = [workitem objectForKey:@"WorkitemId"];    

    [mutableArray removeObjectAtIndex:skipCount];
    [self.projectInfo setValue:mutableArray forKey:@"SortedWorkitems"];
    
    [self.service deleteWorkitem: workitemId];    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)fetchProjectInfoWithProjectId: (NSString *)projectId
{
    [self.loadingIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.service fetchProjectInfoWithProjectId: projectId];
}

- (NSInteger) getNumberOfSections
{
    int rootTaskCount = [((NSArray *)[self.projectInfo objectForKey:@"RootTasks"]) count];
    NSArray *workitems = (NSArray *)[self.projectInfo objectForKey:@"SortedWorkitems"];
    
    if ([[[workitems lastObject] objectForKey:@"RootTaskId"] isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
        rootTaskCount ++;
    }
    return rootTaskCount;
}

- (void)configCell: (WorkitemTableViewCell *)cell AtIndexPath:(NSIndexPath *)indexPath
{
    [cell setupWorkitemInfo: [self getWorkitemFromProjectInfoWithIndexPath:indexPath]];
}



- (IBAction)addWorkitem:(id)sender {
    [self performSegueWithIdentifier:@"Add Workitem" sender:self];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = barButtonItem;    
    
}

- (void)viewDidUnload {
    [self setProjectTaskTableview:nil];
    [self setLoadingIndicatorView:nil];
    [self setAddWorkitemButton:nil];
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = self.projectTitle;
    [self fetchProjectInfoWithProjectId: self.projectId];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Project Workitem Detail"]) {
        WorkitemDetailViewController *workitemDetailViewController = segue.destinationViewController;
        [workitemDetailViewController fetchWorkitemInfoWithWorkitemId: self.selectedWorkitemId];
    } else if([segue.identifier isEqualToString:@"Add Workitem"]){
        UINavigationController *navigationController = segue.destinationViewController;
        AddWorkitemViewController *addWorkitemController = [navigationController.viewControllers lastObject];
        addWorkitemController.projectId = [self.projectInfo valueForKeyPath:@"ProjectInfo.ProjectId"];
        addWorkitemController.sourceType = EzDeskProjectTasksSourceType;
        addWorkitemController.sourceController = self;
    }
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self getNumberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rootTasks = [self.projectInfo objectForKey:@"RootTasks"];
    NSArray *workitems = (NSArray *)[self.projectInfo objectForKey:@"SortedWorkitems"];    
    int rows = 0;
    if (section < [rootTasks count]) {
        NSDictionary *rootTask = [rootTasks objectAtIndex:section];
        rows = [[rootTask objectForKey:@"WorkitemCount"] intValue];
    } else {
        int sum = 0;
        for (NSDictionary *rootTask in rootTasks) {
            sum += [[rootTask objectForKey:@"WorkitemCount"] intValue];
        }
        rows = [workitems count] - sum;
    }
    return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *rootTasks = [self.projectInfo objectForKey:@"RootTasks"];
    if (section < [rootTasks count]) {
        NSDictionary *rootTask = [rootTasks objectAtIndex:section];
        return [rootTask objectForKey:@"Title"];
    } else {
        return @"项目任务";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TaskInProjectCell";
    
    WorkitemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[WorkitemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configCell: cell AtIndexPath:indexPath];
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSInteger oldSectionCount = [self getNumberOfSections];
        // Delete the row from the data source
        [self removeWorkitemInProjectAtIndexPath: indexPath];
        
        // 如果删掉的一个任务是最后一个"没有rootTask的任务"，那么得删掉最后一个Section，
        // 它会导致section 减少 1
        NSInteger newSectionCount = [self getNumberOfSections];        
        if (newSectionCount < oldSectionCount) {
            [self.tableView deleteSections: [NSIndexSet indexSetWithIndex: indexPath.section]withRowAnimation:UITableViewRowAnimationFade];        
        } else {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }   
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *workitem = [self getWorkitemFromProjectInfoWithIndexPath:indexPath];
    self.selectedWorkitemId = [workitem objectForKey:@"WorkitemId"];
    [self performSegueWithIdentifier:@"Show Project Workitem Detail" sender:self];
}


#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID]) {
        self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        self.projectInfo = jsonData;
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID]) {
        self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        [[AppUtil sharedInstance] showNetworkErrorAlert];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECTINFO_WITH_PROJECT_ID]) {
        self.navigationItem.rightBarButtonItem = self.addWorkitemButton;
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    }
}
@end
