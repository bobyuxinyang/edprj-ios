//
//  AppUtil.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtil : NSObject
+ (AppUtil *) sharedInstance;
- (NSURL*)RequestUrlWithFunc: (NSString*) funcUrl;
- (void)showNetworkErrorAlert;
- (void)showAccessFailErrorAlert;
- (void)signOutApplication;
@property (strong, readonly) NSMutableDictionary* sessionDict;
@property (strong, nonatomic) NSString *serviceAddress;
@end
