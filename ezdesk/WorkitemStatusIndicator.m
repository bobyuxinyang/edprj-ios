//
//  WorkitemStatusIndicator.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WorkitemStatusIndicator.h"
#import "UIColor+WebColor.h"

@interface WorkitemStatusIndicator()
@property (nonatomic) WorkitemStatusType status;
@end

@implementation WorkitemStatusIndicator

@synthesize status = _status;

- (id)initIndicatorWithStatus: (WorkitemStatusType) workitemStatusType
{
    self = [super init];
    if (self != nil) {
        self.status = workitemStatusType;
    }
    return self;
}


- (void)drawRectangleInContext: (CGContextRef) context WithColor: (UIColor *)color InRect: (CGRect) rect
{
    UIGraphicsPushContext(context);
    CGContextBeginPath(context);    
    CGContextAddRect(context, rect);
    [color setFill];        
    CGContextFillPath(context);
    UIGraphicsPopContext();        
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *barColor;
    switch (self.status) {
        case WorkitemStatusOnGoing:
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_ON_GOING];
            break;
        case WorkitemStatusFinished:
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_FINISHED];
            break;
        case WorkitemStatusExpired:
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_EXPIRED];
            break;
        case WorkitemStatusWaitingForConfirm:
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_WAITING_FOR_CONFIRM];
            break;            
        case WorkitemStatusReady:
            barColor = [UIColor colorWithWebRGB:COLOR_FOR_WORKITEM_STATUS_READY];
            break;                        
        default:
            barColor = [UIColor whiteColor];
    }
    [self drawRectangleInContext:context WithColor:barColor InRect:rect];
}


@end
