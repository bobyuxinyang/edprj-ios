//
//  ProjectTasksViewController.h
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectTasksViewController : UITableViewController

@property (nonatomic, strong) NSString *projectId;
@property (strong, nonatomic) IBOutlet UITableView *projectTaskTableview;
@property (nonatomic, strong) NSString *selectedWorkitemId;
@property (nonatomic, weak) NSString *projectTitle;
@end
