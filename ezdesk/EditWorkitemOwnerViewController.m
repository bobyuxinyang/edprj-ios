//
//  EditWorkitemOwnerViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EditWorkitemOwnerViewController.h"
#import "EzDeskService.h"
#import "ASIHTTPRequest.h"
#import "WorkitemDetailViewController.h"

@interface EditWorkitemOwnerViewController() <EzDeskServiceDelegate>
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic) EzDeskService *service;
@property (strong, nonatomic) NSArray *userList;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@end

@implementation EditWorkitemOwnerViewController
@synthesize workitemInfo = _workitemInfo;
@synthesize selectedOwnerId = _selectedOwnerId;
@synthesize loadingIndicatorView = _loadingIndicatorView;
@synthesize doneButton = _doneButton;
@synthesize service = _service;
@synthesize userList = _userList;
@synthesize selectedIndexPath = _selectedIndexPath;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)setSelectedOwnerId:(NSString *)selectedOwnerId
{
    if ([selectedOwnerId isEqual: [NSNull null]]) {
        selectedOwnerId = nil;
    }
    if (![_selectedOwnerId isEqualToString:selectedOwnerId]) {               
        _selectedOwnerId = selectedOwnerId;
    }        
}

- (void)setUserList:(NSArray *)userList 
{
    if (![userList isEqualToArray:_userList]) {
        _userList = userList;
        [self.tableView reloadData];               
        
        
        NSLog(@"%@", self.selectedOwnerId);
        
        if (self.selectedOwnerId != nil) {
            // find the selected IndexPath
            BOOL foundUser = NO;
            NSUInteger sectionCount = 0, rowCount = 0;
            // set selectedIndexPath
            for (NSDictionary *deptInfo in userList) {
                rowCount = 0;                
                for (NSDictionary *userInfo in [deptInfo objectForKey: @"Users"]) {
                    NSLog(@"%@", userInfo);
                    if ([self.selectedOwnerId isEqualToString: [userInfo objectForKey: @"UserId"]]) {
                        foundUser = YES;
                        break;
                    }
                    rowCount ++;
                }
                if (foundUser) {
                    break;
                }
                sectionCount ++;
            }
            if (foundUser) {
                self.selectedIndexPath = [NSIndexPath indexPathForRow: rowCount inSection: sectionCount];
                [self.tableView scrollToRowAtIndexPath: self.selectedIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
        }
    }
}

- (void)fetchWorkitemOwnerCandidateList
{
    [self.loadingIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.service fetchWorkitemOwnerCandidateList];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)editWorkitemOwner:(id)sender {
    [self.service updateWorkitemOwner:self.selectedOwnerId
                       WithWorkitemId:[self.workitemInfo objectForKey: @"WorkitemId"]];
}

#pragma mark - View lifecycle


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchWorkitemOwnerCandidateList];
}

- (void)viewDidAppear:(BOOL)animated 
{
    [super viewDidAppear:animated];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.userList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.userList objectAtIndex: section] objectForKey: @"Users"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"User Info Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    NSDictionary *userInfo = [[[self.userList objectAtIndex: indexPath.section] objectForKey: @"Users"] objectAtIndex: indexPath.row];
    cell.textLabel.text = [userInfo objectForKey: @"Title"];
    cell.detailTextLabel.text = [userInfo objectForKey: @"PositionName"];
    if ([[userInfo objectForKey: @"UserId"] isEqualToString: self.selectedOwnerId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;  
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.userList objectAtIndex: section] objectForKey: @"DeptName"];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *userInfo = [[[self.userList objectAtIndex: indexPath.section] objectForKey: @"Users"] objectAtIndex: indexPath.row];
    NSString *userId = [userInfo objectForKey: @"UserId"];
    [self.tableView cellForRowAtIndexPath: self.selectedIndexPath].accessoryType = UITableViewCellAccessoryNone;        
    if ([userId isEqualToString: self.selectedOwnerId]) {    
        self.selectedOwnerId = nil;
        self.selectedIndexPath = nil;
    } else {
        [self.tableView cellForRowAtIndexPath: indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedOwnerId = userId;
        self.selectedIndexPath = indexPath;   
    }    
    [self.tableView deselectRowAtIndexPath: indexPath animated:YES];
}


#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_OWNER]) {
        int count = [self.navigationController.viewControllers count];
        WorkitemDetailViewController *workitemDetailViewController = [self.navigationController.viewControllers objectAtIndex:count - 2];
        [workitemDetailViewController fetchWorkitemInfoWithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]]; 
        [self.navigationController popViewControllerAnimated: YES];
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_OWNER_CANDIDATE]) {
        self.navigationItem.rightBarButtonItem = self.doneButton;
        self.userList = jsonData;
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_OWNER]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];        
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_OWNER_CANDIDATE]) {
        self.navigationItem.rightBarButtonItem = self.doneButton;  
        [[AppUtil sharedInstance] showNetworkErrorAlert];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_OWNER]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        [self.navigationController popViewControllerAnimated: YES];        
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_OWNER_CANDIDATE]) {
        self.navigationItem.rightBarButtonItem = self.doneButton;  
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    }
}


- (void)viewDidUnload {
    [self setLoadingIndicatorView:nil];
    [self setDoneButton:nil];
    [super viewDidUnload];
}
@end
