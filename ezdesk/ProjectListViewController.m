//
//  ProjectListViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-1-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProjectListViewController.h"
#import "ProjectTableViewCell.h"
#import "ProjectTasksViewController.h"
#import "EzDeskService.h"

@interface ProjectListViewController() <EzDeskServiceDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *projectList;
@property (nonatomic, strong) NSDictionary *selectedProject;
@property (nonatomic, strong) EzDeskService *service;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;

-(void) fetchProjectList;
@end

@implementation ProjectListViewController

@synthesize tableView = _tableView;
@synthesize projectList = _projectList, selectedProject = _selectedProject;
@synthesize service = _service;
@synthesize loadingIndicatorView = _loadingIndicatorView;

- (EzDeskService *)service
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

#pragma mark - Properties
-(void) setProjectList:(NSArray *)projectList
{
    if (projectList != _projectList) {
        _projectList = projectList;
    }
    [self.tableView reloadData];
}

- (void) fetchProjectList
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.loadingIndicatorView startAnimating];
    [self.service fetchProjectList];
}

- (void) configCell: (ProjectTableViewCell*) cell AtIndex: (NSIndexPath*) indexPath
{
    NSDictionary *projectInfo = [self.projectList objectAtIndex:indexPath.row];
    [cell setupProjectInfo: projectInfo];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = barButtonItem;    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // get project List
    [self fetchProjectList];
}


- (void)viewDidUnload {
    [self setTableView:nil];
    [self setLoadingIndicatorView:nil];
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.projectList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProjectInfo Cell";
    
    ProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ProjectTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configCell: cell AtIndex: indexPath];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedProject = [self.projectList objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"Show Project Tasks" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Project Tasks"] && self.selectedProject) {
        ProjectTasksViewController *projectTasksViewController = segue.destinationViewController;
        projectTasksViewController.projectId = [self.selectedProject objectForKey:@"Id"];
        // 先把项目名称传过去，体验更好。。
        projectTasksViewController.projectTitle = [self.selectedProject objectForKey: @"Title"]; 
    }    
}


#pragma mark -- EzDesk Service Delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECT_LIST]) {
        self.projectList = jsonData;
        [self.loadingIndicatorView stopAnimating];                
        self.navigationItem.rightBarButtonItem = nil;
    }
}



- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECT_LIST]) {            
        self.navigationItem.rightBarButtonItem = nil;
        [[AppUtil sharedInstance] showNetworkErrorAlert];
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_FETCH_PROJECT_LIST]) {            
        self.navigationItem.rightBarButtonItem = nil;
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
    }

}

@end
