//
//  EditProjectAndTaskTableViewController.m
//  ezdesk
//
//  Created by 杨裕欣 on 12-2-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EditProjectAndTaskTableViewController.h"
#import "ASIHTTPRequest.h"
#import "EzDeskService.h"
#import "WorkitemDetailViewController.h"

#define PROJECT_SELECTOR_INDEX 0
#define ROOTTASK_SELECTOR_INDEX 1

@interface EditProjectAndTaskTableViewController() <EzDeskServiceDelegate>
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *projectAndTaskSelector;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (nonatomic, strong) NSArray* projectList;
@property (nonatomic, strong) NSArray* rootTaskList;
- (void)setupProjectAndRootTasks;
@property (strong, nonatomic) EzDeskService *service;
- (void)fetchWorkitemRootTasksCandidate;
@end

@implementation EditProjectAndTaskTableViewController
@synthesize loadingIndicatorView = _loadingIndicatorView;
@synthesize projectAndTaskSelector = _projectAndTaskSelector;
@synthesize doneButton = _doneButton;
@synthesize workitemInfo = _workitemInfo;
@synthesize selectedTaskId = _selectedTaskId, selectedProjectId = _selectedProjectId;
@synthesize projectList = _projectList, rootTaskList = _rootTaskList;
@synthesize service = _service;

- (EzDeskService *)service 
{
    if (_service == nil) {
        _service = [[EzDeskService alloc] init];
        _service.delegate = self;
    }
    return _service;
}

- (void)setProjectList:(NSArray *)projectList
{
    if (![_projectList isEqualToArray: projectList]) {
        _projectList = projectList;
        if (self.projectAndTaskSelector.selectedSegmentIndex == PROJECT_SELECTOR_INDEX) {
            [self.tableView reloadData];
            
            NSInteger selectedIndex = [self.projectList indexOfObjectPassingTest: ^(id element, NSUInteger idx, BOOL * stop){
                NSDictionary *projectInfo = element;
                *stop = [[projectInfo objectForKey: @"ProjectId"] isEqualToString: self.selectedProjectId];
                return *stop;
            }];
            [self.tableView scrollToRowAtIndexPath: [NSIndexPath indexPathForRow: selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    }
}

- (void)setRootTaskList:(NSArray *)rootTaskList
{
    if (![_rootTaskList isEqualToArray: rootTaskList]) {
        _rootTaskList = rootTaskList;
        if (self.projectAndTaskSelector.selectedSegmentIndex == ROOTTASK_SELECTOR_INDEX) {
            [self.tableView reloadData];
        }
    }
}

- (void)setSelectedProjectId:(NSString *)selectedProjectId
{
    if (![_selectedProjectId isEqualToString:selectedProjectId]) {        
        // 处理表格
        if (self.projectAndTaskSelector.selectedSegmentIndex == PROJECT_SELECTOR_INDEX) {
            
            NSInteger oldIndex = [self.projectList indexOfObjectPassingTest: ^(id element, NSUInteger idx, BOOL * stop){
                NSDictionary *projectInfo = element;
                *stop = [[projectInfo objectForKey: @"ProjectId"] isEqualToString: _selectedProjectId];
                return *stop;
            }];
            
            NSInteger newIndex = [self.projectList indexOfObjectPassingTest: ^(id element, NSUInteger idx, BOOL * stop){
                NSDictionary *projectInfo = element;
                *stop = [[projectInfo objectForKey: @"ProjectId"] isEqualToString: selectedProjectId];
                return *stop;
            }];
            
            [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:oldIndex inSection:0]].accessoryType = UITableViewCellAccessoryNone;
            [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:newIndex inSection:0]].accessoryType = UITableViewCellAccessoryCheckmark;            
        }
        
        _selectedProjectId = selectedProjectId;
        _selectedTaskId = nil;
        
        [self fetchWorkitemRootTasksCandidate];
    }
}

- (void)setSelectedTaskId:(NSString *)selectedTaskId
{
    if (![_selectedTaskId isEqualToString:selectedTaskId]) {        
        // 处理表格
        if (self.projectAndTaskSelector.selectedSegmentIndex == ROOTTASK_SELECTOR_INDEX) {
            
            NSInteger oldIndex = [self.rootTaskList indexOfObjectPassingTest: ^(id element, NSUInteger idx, BOOL * stop){
                *stop = [[element objectForKey: @"TaskId"] isEqualToString: _selectedTaskId];
                return *stop;
            }];
            
            NSInteger newIndex = [self.rootTaskList indexOfObjectPassingTest: ^(id element, NSUInteger idx, BOOL * stop){
                *stop = [[element objectForKey: @"TaskId"] isEqualToString: selectedTaskId];
                return *stop;
            }];
            
            [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:oldIndex inSection:0]].accessoryType = UITableViewCellAccessoryNone;
            [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:newIndex inSection:0]].accessoryType = UITableViewCellAccessoryCheckmark;            
        }
        _selectedTaskId = selectedTaskId;
    }    
}

- (void)fetchWorkitemProjectCandidate
{
    [self.loadingIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];
    [self.service fetchWorkitemProjectCandidateList];
}

- (void)fetchWorkitemRootTasksCandidate
{
    if (self.selectedProjectId.length > 0) {
        
        [self.loadingIndicatorView startAnimating];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.loadingIndicatorView];        
        [self.service fetchWorkitemRootTaskCandidateListWithProjectId: self.selectedProjectId];
        
    } else {
        [self.projectAndTaskSelector setEnabled:NO forSegmentAtIndex: ROOTTASK_SELECTOR_INDEX];
        self.projectAndTaskSelector.selectedSegmentIndex = PROJECT_SELECTOR_INDEX;
    }
}

- (void)setupProjectAndRootTasks
{
    [self fetchWorkitemProjectCandidate];
    [self fetchWorkitemRootTasksCandidate];          
}

- (IBAction)selectProjectAndTask:(id)sender {
    [self.tableView reloadData];
}

- (IBAction)editWorkitemProjectAndTask:(id)sender {
    [self.service updateWorkitemProjectId:self.selectedProjectId 
                                AndTaskId:self.selectedTaskId 
                           WithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupProjectAndRootTasks];
}

- (void)viewDidUnload
{
    [self setProjectAndTaskSelector:nil];
    [self setLoadingIndicatorView:nil];
    [self setDoneButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.projectAndTaskSelector.selectedSegmentIndex == PROJECT_SELECTOR_INDEX) {
        return [self.projectList count];
    } else if (self.projectAndTaskSelector.selectedSegmentIndex == ROOTTASK_SELECTOR_INDEX){
        return [self.rootTaskList count];
    }       
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    CellIdentifier = @"Title Cell";    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if (self.projectAndTaskSelector.selectedSegmentIndex == PROJECT_SELECTOR_INDEX) {
        NSDictionary *project = [self.projectList objectAtIndex: indexPath.row];
        cell.textLabel.text = [project objectForKey: @"Title"];       
        if ([[project objectForKey:@"ProjectId"] isEqualToString: self.selectedProjectId]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if (self.projectAndTaskSelector.selectedSegmentIndex == ROOTTASK_SELECTOR_INDEX){
        NSDictionary *task = [self.rootTaskList objectAtIndex: indexPath.row];
        cell.textLabel.text = [task objectForKey: @"Title"];     
        if ([[task objectForKey: @"TaskId"] isEqualToString: self.selectedTaskId]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.projectAndTaskSelector.selectedSegmentIndex == PROJECT_SELECTOR_INDEX) {
        NSDictionary *project = [self.projectList objectAtIndex: indexPath.row];
        NSString *projectId = [project objectForKey: @"ProjectId"];
        if ([projectId isEqualToString:self.selectedProjectId]) {
            self.selectedProjectId = nil;
        } else {
            self.selectedProjectId = projectId;
        }
    } else if (self.projectAndTaskSelector.selectedSegmentIndex == ROOTTASK_SELECTOR_INDEX){
        NSDictionary *rootTask = [self.rootTaskList objectAtIndex: indexPath.row];
        NSString *taskId = [rootTask objectForKey: @"TaskId"];
        if ([taskId isEqualToString:self.selectedTaskId]) {
            self.selectedTaskId = nil;
        } else {
            self.selectedTaskId = taskId;
        }
    }    
    [self.tableView deselectRowAtIndexPath: indexPath animated:YES];
}

#pragma mark - EzDesk Service delegate

- (void)service:(EzDeskService *)service AccessSuccessWithData: (id)jsonData WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PROJECT_AND_TASK]) {
        int count = [self.navigationController.viewControllers count];
        WorkitemDetailViewController *workitemDetailViewController = [self.navigationController.viewControllers objectAtIndex:count - 2];
        [workitemDetailViewController fetchWorkitemInfoWithWorkitemId: [self.workitemInfo objectForKey: @"WorkitemId"]]; 
        [self.navigationController popViewControllerAnimated: YES];
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE]) {
        self.projectList = jsonData;
        self.navigationItem.rightBarButtonItem = self.doneButton;
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE]) {
        self.rootTaskList = jsonData;
        self.navigationItem.rightBarButtonItem = self.doneButton;        
        
        [self.projectAndTaskSelector setEnabled:YES forSegmentAtIndex: ROOTTASK_SELECTOR_INDEX]; 
        
        if ([self.rootTaskList count] == 0) {
            self.projectAndTaskSelector.selectedSegmentIndex = PROJECT_SELECTOR_INDEX;
        }        
    }
}

- (void)service:(EzDeskService *)service NetworkError:(NSError *)error WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PROJECT_AND_TASK]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];        
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        self.navigationItem.rightBarButtonItem = self.doneButton;        
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];
        self.navigationItem.rightBarButtonItem = self.doneButton;        
    }
}

- (void)service:(EzDeskService *)service WithAccessFail:(EzDeskAccessFailType)failType AndDescription:(NSString *)description WithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString: EZ_UPDATE_WORKITEM_PROJECT_AND_TASK]) {
        [[AppUtil sharedInstance] showNetworkErrorAlert];  
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_PROJECT_CANDIDATE]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        self.navigationItem.rightBarButtonItem = self.doneButton;        
    } else if ([identifier isEqualToString: EZ_FETCH_WORKITEM_ROOTTASKS_CANDIDATE]) {
        [[AppUtil sharedInstance] showAccessFailErrorAlert];
        self.navigationItem.rightBarButtonItem = self.doneButton;        
    }
}

@end
